#ifndef __RASHPA_ROCE_UC_H_INCLUDED__
#define __RASHPA_ROCE_UC_H_INCLUDED__

#define CQ_SEND 1
#define CQ_RECV 0

typedef struct _roce_ctx_t 
{
  struct ibv_device **dev_list;
  struct ibv_device *ib_dev;
  struct ibv_context *context;
  struct ibv_comp_channel *channel;
  struct ibv_pd *pd;
  struct ibv_mr *mr;
  struct ibv_mr *mr_gth;
  struct ibv_cq *cq[RASHPA_LINK_NUM_MAX];
  struct ibv_ah *ah;

  int qkey;
  int gid_index;
  int dlid;
  int is_ib;
  int remote_qpn[RASHPA_LINK_NUM_MAX];
  int rkey[RASHPA_LINK_NUM_MAX];
  void *daddr[RASHPA_LINK_NUM_MAX];
  struct ibv_qp *qp[RASHPA_LINK_NUM_MAX];
  void *buf_gth;
  pthread_t thread_id[RASHPA_LINK_NUM_MAX];
  int service_type; // UD UC
} roce_ctx_t;

enum release_ib_action {
  RELEASE_RESET,
  RELEASE_IBV_POLL,
  RELEASE_IBV_DESTROY_QP,
  RELEASE_IBV_DESTROY_CQ,
  RELEASE_IBV_DEREG_MR,
  RELEASE_IBV_DEALLOC_PD,
  RELEASE_IBV_DESTROY_COMP_CHANNEL,
  RELEASE_IBV_CLOSE_DEVICE,
  RELEASE_IBV_FREE_DEVICE_LIST,
  RELEASE_GTH_MEM
};

extern int rashpa_rocev2_uc_backend_receiver_init(int roi_id);
extern int rashpa_rocev2_uc_detector_init(int roi_id);
extern void *rashpa_rocev2_uc_detector_mainloop(void *arg);
extern void *rashpa_rocev2_uc_detector_mainloop_single_thread(void *arg);
extern void *rashpa_rocev2_uc_backend_receiver_mainloop(void *arg);
extern void *
rashpa_rocev2_uc_backend_receiver_mainloop_single_thread(void *arg);
extern void release_ib_context(int, int);
extern void create_ib_context();
extern void allocate_gpu_device_memory();
extern void select_cpu(int);

extern volatile int statusFlag;
extern volatile int *pstatusFlag;
#endif
