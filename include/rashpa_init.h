
#ifndef __RASHPA_INIT_H_INCLUDED__
#define __RASHPA_INIT_H_INCLUDED__

#include <time.h>
extern void displayProfilingResult(const char *msg, struct timespec *end,
                                   struct timespec *start, int, int, int);
// extern "C" void saveDataInFile(const char*fname,void* buffer,size_t len);
extern unsigned long long rdtsc(void);

extern const char *RASHPA_LINK_NAME[];
extern const char *ROMULU_RECEIVER[];
extern const char *RASHPA_XML_ERRORS[];
extern const char *ROMULU_STATE[];
extern const char *REMU_STATE[];
// extern rashpa_ctx_t g_rashpa;

#endif
