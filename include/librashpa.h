/*
 * librashpa.h
 *
 * Copyright 2018  <ponsard@linkarkouli>
 *
 *
 *
 *
 */

#ifndef __RASHPALIB_H_INCLUDED__
#define __RASHPALIB_H_INCLUDED__

#define DEBUG 1
#define debug_printf      if (DEBUG) printf

// size max
#define HOST_NAME_MAX 64
#define REMOTE_HOST_PORT_MAX 5
#define WIRE_NAME_MAX 16
#define HUGE_PAGE_NAME_MAX 32
#define RASHPA_LINK_NUM_MAX 8
#define RASHPA_INFO_MSG_SIZE 1024
#define RASHPA_TELEGRAM_MSG_SIZE 4096

// emulator internal states
#define REMU_IDLE 0
#define REMU_SOURCE_CONFIGURED 1
#define REMU_LINK_READY 2
#define REMU_RUNNING 3

// romulu internal states
#define ROMULU_IDLE 0
#define ROMULU_RASHPA_CONFIGURED 1
#define ROMULU_LINK_READY 2
#define ROMULU_GPU_READY 3
#define ROMULU_RUNNING 4

// location
#define RASHPA_DETECTOR 1
#define RASHPA_BACKEND 2
#define RASHPA_BACKENDGPU 3

// links
#define RASHPA_LINK_NONE 0
#define RASHPA_LINK_GDRCOPY 1
#define RASHPA_LINK_RSOCKET 2
#define RASHPA_LINK_ROCEV2_RAW 3
#define RASHPA_LINK_ROCEV2_UC 4
#define RASHPA_LINK_MEMCOPY 5
#define RASHPA_LINK_ZMQ 6
#define RASHPA_LINK_UDP 7
#define RASHPA_LINK_UDP_JF 8

// receiver
#define RASHPA_RECEIVER_NVIDIA_CUDA_GPU 0
#define RASHPA_RECEIVER_AMD_GPU 1
#define RASHPA_RECEIVER_OPENCL_XEON 2
#define RASHPA_RECEIVER_NVIDIA_OPENCL_GPU 3
#define RASHPA_RECEIVER_NVIDIA_CUDAMEM_GPU 4
#define RASHPA_RECEIVER_NOGPU 5

// statusFlag
#define GPUH2D 10
#define GPUBUSY 20
#define GPUD2H 30
#define GPUIDLE 40
// waitFlag
#define KERNELREADY 2222
#define KERNELBUSY 1110

extern volatile int gpustate;

typedef struct _rashpa_ctx {
  // uuid of the link
  //	int					rashpa_id;
  char messages[RASHPA_INFO_MSG_SIZE];
  // application internal state
  int remu_state;
  int romulu_state;

  // datachannel thread id
  pthread_t rashpa_tid;
  pthread_t gpu_tid;

  pthread_cond_t condition;
  pthread_mutex_t mutex;
  int nImgOnGpu;
  int gpucode;   // select kerne algorithm
  int gpustream; // select stream number

  // remu data source
  int data_source;

  // rashpa transfer
  int receiver_type;
  int link_type;
  int gpuId;

  // NIC name
  char nic[WIRE_NAME_MAX];

  // remote host
  char hostname[HOST_NAME_MAX];
  int port;
  int computeProcessPid; // rsocket

  // sizes
  size_t dsSize;
  int bdepth;
  int hsz;
  int vsz;
  int savePinnedBuf;

  int nImages;
  int nIter;
  int rashpa_bunch; // number of transmitted image at once (limited by queue
                    // rwr_max)
  int batch;        // number multiplier images on gpu

  int64_t nanodelay;

  // rashpa buffer
  int offset; // Start Of Frame
  void *dest_addr;
  void *source_addr;
  void *h_output1;
  void *h_output2;
  void *h_output11;
  void *h_output22;

  // roi for rashpa_id
  // FIXME actually src and dst never used in the same application
  int roi_stride;
  int gpu_stride;
  int roi_number;
  int roi_src_x[RASHPA_LINK_NUM_MAX];
  int roi_src_y[RASHPA_LINK_NUM_MAX];
  int roi_dst_x[RASHPA_LINK_NUM_MAX];
  int roi_dst_y[RASHPA_LINK_NUM_MAX];
  int roi_w[RASHPA_LINK_NUM_MAX];
  int roi_h[RASHPA_LINK_NUM_MAX];

  // int					kernel_w;
  // int					kernel_h;
  int gpu_w;
  int gpu_h;
  // int 				veto_h;
  // int 				veto_l;

  char gain_file[HOST_NAME_MAX];
} rashpa_ctx_t;

// errors
#define RASHPA_XML_ERROR 0
#define RASHPA_XML_BAD_LINK 1
#define RASHPA_XML_BAD_RECEIVER 2
#define RASHPA_XML_OK 3
#define RASHPA_LINK_ERROR 4
#define RASHPA_LINK_OK 6

extern rashpa_ctx_t g_rashpa;

#endif
