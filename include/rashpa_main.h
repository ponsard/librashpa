#ifndef __RASHPA_MAIN_H_INCLUDED__
#define __RASHPA_MAIN_H_INCLUDED__

#define A_C_RED "\x1b[31m"
#define A_C_RESET "\x1b[0m"
#define A_C_GRN "\x1B[32m"
#define A_C_YEL "\x1B[33m"
#define A_C_BLU "\x1B[34m"
#define A_C_MAG "\x1B[35m"
#define A_C_CYN "\x1B[36m"
#define A_C_WHT "\x1B[37m"

extern void rashpa_fini(int dir);
extern int rashpa_init(int dir);
extern void *rashpa_receiver_mainloop(void *arg);
extern void *rashpa_detector_mainloop(void *arg);
// extern void 	rashpa_buffer_init(void* addr, int w);

void (*rashpa_recv_line)(void *buffer, int len);
void (*rashpa_buffer_init)(void *, int);
int (*rashpa_recv_lines)(void *buffer, int len, int n);
void (*rashpa_send_line)(void *buffer, int len);
// extern void 	select_cpu(int dir);
// extern void* 	rashpa_JF_receiver_mainloop(void* arg);
// extern void* 	rashpa_JF_detector_mainloop(void* arg);

#endif
