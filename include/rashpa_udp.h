#ifndef __RASHPA_UDP_H_INCLUDED__
#define __RASHPA_UDP_H_INCLUDED__

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>

typedef struct _udp_ctx_t {
  int s;
  int port;
  struct sockaddr_in dst_adr;
} udp_ctx_t;

extern int rashpa_udp_ini();
extern void rashpa_udp_fini();
extern void rashpa_udp_send_line(void *buffer, int width);
extern int rashpa_udp_recv_lines(void *buffer, int width, int n);
extern void rashpa_udp_recv_line(void *buffer, int width);
extern void rashpa_udp_buffer_init(void *addr, int w);

#define VLEN 32
extern struct mmsghdr msgs[VLEN];
extern struct iovec iovecs[VLEN];

#endif
