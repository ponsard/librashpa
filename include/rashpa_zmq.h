#ifndef __RASHPA_ZMQ_H_INCLUDED__
#define __RASHPA_ZMQ_H_INCLUDED__

//#define RASHPA_ZMQ_SUB 			1
//#define RASHPA_ZMQ_PUB			2
//#define RASHPA_ZMQ_PULL		 	3
//#define RASHPA_ZMQ_PUSH 		4

typedef struct _zmq_ctx_t {

  void *context;
  void *pull; // receiver
  void *push; // detector

  // FIXME required ?
  //	void *buf;
  //	int len;
} zmq_ctx_t;

extern void *rashpa_zmq_detector_mainloop(void *);
extern void *rashpa_zmq_backend_receiver_mainloop(void *);
extern int rashpa_zmq_init(int dir);
extern void rashpa_zmq_fini(int);

extern void rashpa_zmq_send_line(void *buffer, int width);
extern void rashpa_zmq_recv_line(void *buffer, int width);
extern int rashpa_zmq_recv_lines(void *buffer, int width, int n);
extern void rashpa_zmq_buffer_init(void *, int w);

extern zmq_ctx_t g_zmq;

#endif
