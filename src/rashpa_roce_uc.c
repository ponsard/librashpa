/*
 *
 *
 *
 *
 *
 *
 *
 */
#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE

#include <arpa/inet.h>
#include <fcntl.h>
#include <infiniband/verbs.h>
#include <malloc.h>
#include <netdb.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "librashpa.h"
#include "rashpa_init.h"
#include "rashpa_main.h"
#include "rashpa_roce_uc.h"

#define SYNCHRO 0
#define MULTI_MODULES 0
#define NMODULES 1

// FIXME somewhere else

volatile int gpustate;

roce_ctx_t g_roce;
rashpa_ctx_t g_rashpa;

void create_ib_context() {
  roce_ctx_t *roce = &g_roce;
  rashpa_ctx_t *link = &g_rashpa;
  int ib_port = 1;

  int num_devices;
  /* kk*/

  // get infiniband device

  debug_printf("ibv_get_device_list, searching %s\n", link->nic);
  roce->dev_list = ibv_get_device_list(&num_devices);
  if (!roce->dev_list) {
    perror("Failed to get IB devices list");
    return;
  }

  roce->ib_dev = NULL;
  for (int i = 0; roce->dev_list[i]; i++)
    if (strcmp(ibv_get_device_name(roce->dev_list[i]), link->nic) == 0) {
      roce->ib_dev = roce->dev_list[i];
      break;
    }
  if (!roce->ib_dev) {
    fprintf(stderr, "No IB devices found on wire %s\n", link->nic);
    release_ib_context(RELEASE_IBV_FREE_DEVICE_LIST, 0);
    return;
  }

  // create context
  // printf("ibv_open_device\n");
  roce->context = ibv_open_device(roce->ib_dev);
  if (!roce->context) {
    fprintf(stderr, "Couldn't get context for %s\n",
            ibv_get_device_name(roce->ib_dev));
    release_ib_context(RELEASE_IBV_CLOSE_DEVICE, 0);
    return;
  }

  struct ibv_port_attr port_info = {};
  if (ibv_query_port(roce->context, ib_port, &port_info)) {
    fprintf(stderr, "Unable to query port info for port %d\n", ib_port);
    release_ib_context(RELEASE_IBV_CLOSE_DEVICE, 0);
    return;
  }
  int mtu = 1 << (port_info.active_mtu + 7);

  // Protection Domain
  // debug_printf("ibv_alloc_pd\n");
  roce->pd = ibv_alloc_pd(roce->context);
  if (!roce->pd) {
    fprintf(stderr, "Couldn't allocate PD\n");
    release_ib_context(RELEASE_IBV_DESTROY_COMP_CHANNEL, 0);
    return;
  }

  // we do not need channel (for events mode)
  roce->channel = NULL;

  debug_printf("created ib_context for device %s (MTU %d)\n",
               ibv_get_device_name(roce->ib_dev), mtu);
}

void release_ib_context(int action, int i) {
  roce_ctx_t *roce = &g_roce;
  int num_comp;
  int j = 0;
  struct ibv_wc wc;
  struct ibv_qp_attr attr;

  sleep(1); // wait for completion outsandirng requests

  switch (action) {

  case RELEASE_IBV_POLL:
    debug_printf("clear ibv_poll_cq %d\n", i);
    do {
      j++;
      num_comp = ibv_poll_cq(roce->cq[i], 1, &wc);
    } while (num_comp > 0 || j < 60000); // > wq max 326536
  case RELEASE_RESET:
    debug_printf("ibv_modify_qp to RESET\n");
    attr.qp_state = IBV_QPS_RESET;
    if (ibv_modify_qp(roce->qp[i], &attr, IBV_QP_STATE)) {
      debug_printf("Failed to modify QP to RESET\n");
    }
  case RELEASE_IBV_DESTROY_QP:
    debug_printf("ibv_destroy_qp %d\n", i);
    if (ibv_destroy_qp(roce->qp[i])) {
      debug_printf("Error, ibv_destroy_qp() failed\n");
      return;
    }

  case RELEASE_IBV_DESTROY_CQ:
    if (i == 0) {
      debug_printf("ibv_destroy_cq %d\n", i);
      if (ibv_destroy_cq(roce->cq[i])) {
        debug_printf("Error, ibv_destroy_cq() failed\n");
      }
    }
    debug_printf("ibv_destroy_cq DONE\n");

  case RELEASE_IBV_DEREG_MR:
    if (i == 0) {
      debug_printf("ibv_dereg_mr_gth no implemented\n");
      // if () ibv_dereg_mr(roce->mr_gth);
      debug_printf("ibv_dereg_mr\n");

      // if (roce->mr)
      ibv_dereg_mr(roce->mr);
    }

  case RELEASE_IBV_DEALLOC_PD:
    if (i == 0) {
      debug_printf("ibv_dealloc_pd\n");
      ibv_dealloc_pd(roce->pd);
    }

  case RELEASE_IBV_DESTROY_COMP_CHANNEL:
    if (i == 0) {
      debug_printf("ibv_destroy_comp_channel\n");
      if (roce->channel)
        ibv_destroy_comp_channel(roce->channel);
    }

  case RELEASE_IBV_CLOSE_DEVICE:
    if (i == 0) {
      debug_printf("ibv_close_device\n");
      ibv_close_device(roce->context);
    }

  case RELEASE_IBV_FREE_DEVICE_LIST:
    if (i == 0) {
      debug_printf("ibv_free_device_list\n");
      ibv_free_device_list(roce->dev_list);
    }

  }
}

int create_ib_cq_qprtr(int direction, int roi_id) {
  roce_ctx_t *roce = &g_roce;
  rashpa_ctx_t *link = &g_rashpa;
  int service_type = roce->service_type;
  int ib_port = 1;
  int h = link->roi_h[roi_id];
  int bunch = link->rashpa_bunch;
  int gid_index = roce->gid_index;
  int dlid = roce->dlid;
  int is_IB = roce->is_ib;
  char *hostname = &link->hostname[0];

  debug_printf(
      "ibv_create_cq of h[%d] = %d  bunch %d hostname %s QP type: %s\n", roi_id,
      h, bunch, hostname,
      service_type == IBV_QPT_UC ? "IBV_QPT_UC" : "IBV_QPT_RAW_ETHERNET");

  // FIXME choose relevant cqe size for completion events : we read h line at a
  // time
  roce->cq[roi_id] = ibv_create_cq(roce->context, bunch / 2 * h, NULL, NULL, 0);

  if (!roce->cq[roi_id]) {
    debug_printf("Couldn't create CQ (is h to high?) %d\n", h);
    release_ib_context(RELEASE_IBV_DEREG_MR, roi_id);
    return RASHPA_LINK_ERROR;
  }
  // odao2 vs scisoft14
  int max_recv_wr = is_IB ? 16351 : 32768;
  // Queue Pair
  struct ibv_qp_init_attr qp_init_attr;
  memset(&qp_init_attr, 0, sizeof(struct ibv_qp_init_attr));
  qp_init_attr.send_cq = roce->cq[roi_id];
  qp_init_attr.recv_cq = roce->cq[roi_id];
  qp_init_attr.cap.max_send_wr = direction == RASHPA_DETECTOR ? 2 * h : 2;
  qp_init_attr.cap.max_send_sge = 2;
  qp_init_attr.cap.max_recv_wr =
      (direction == RASHPA_BACKEND ? max_recv_wr : 2); // 16351 connectx3 16384
  qp_init_attr.cap.max_recv_sge = 2;

  // Service Type
  qp_init_attr.qp_type =
      (service_type == IBV_QPT_UC) ? IBV_QPT_UC : IBV_QPT_RAW_PACKET; // for RAW

  debug_printf("ibv_create_qp %s on device %s with max_send_wr =%d\n",
               direction == RASHPA_DETECTOR ? "send" : "recv", link->nic,qp_init_attr.cap.max_send_wr);
  roce->qp[roi_id] = ibv_create_qp(roce->pd, &qp_init_attr);
  if (!roce->qp[roi_id]) {
    debug_printf("Error, ibv_create_qp() failed. see qp_init_attr.cap.*_max\n");
    release_ib_context(RELEASE_IBV_DESTROY_CQ, roi_id);
    return RASHPA_LINK_ERROR;
  }

  debug_printf("ibv_modify_qp to INIT qpn %d\n", roce->qp[0]->qp_num);
  struct ibv_qp_attr attr;
  memset(&attr, 0, sizeof(struct ibv_qp_attr));
  attr.qp_state = IBV_QPS_INIT;
  attr.pkey_index = 0;
  attr.port_num = ib_port;

  int flag = IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT;

  // required for UC
  attr.qp_access_flags = IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE;
  flag |= IBV_QP_ACCESS_FLAGS;

  if (ibv_modify_qp(roce->qp[roi_id], &attr, flag)) {
    debug_printf("Failed to modify QP to INIT\n");
    release_ib_context(RELEASE_IBV_DESTROY_QP, roi_id);
    return RASHPA_LINK_ERROR;
  }

  debug_printf("ibv_modify_qp to RTR\n");
  memset(&attr, 0, sizeof(struct ibv_qp_attr));
  union ibv_gid gid;
  // FIXME HARD CODED : select RocEv1,v2
  // use show_gids to select index
  if (ibv_query_gid(roce->context, ib_port, gid_index, &gid)) {
    fprintf(stderr, "ibv_query_gid error\n");
    return -1;
  }
  debug_printf("%x%x%x%x%x%x%x%x%x%x\n", gid.raw[0], gid.raw[1], gid.raw[2],
         gid.raw[3], gid.raw[4], gid.raw[5], gid.raw[6], gid.raw[7], gid.raw[8],
         gid.raw[9]);
  debug_printf("%x%x%d.%d.%d.%d\n", gid.raw[10], gid.raw[11], gid.raw[12],
         gid.raw[13], gid.raw[14], gid.raw[15]);

  flag = IBV_QP_STATE;
  attr.qp_state = IBV_QPS_RTR;

  debug_printf("UC queue pair %s\n",
               direction == RASHPA_DETECTOR ? "DETECTOR" : "RECEIVER");
  if (direction == RASHPA_DETECTOR) {
    // Mandatory for UC
    attr.dest_qp_num =
        roce->remote_qpn[roi_id]; // remote_qpn; //required only on source
    if (!is_IB) {
      *(int *)(gid.raw + 12) = inet_addr(hostname);
      debug_printf("UC WRITE to %s dest ip addr %x%x %d.%d.%d.%d\n", hostname,
             gid.raw[10], gid.raw[11], gid.raw[12], gid.raw[13], gid.raw[14],
             gid.raw[15]);
    }
  } else { // receiver
    if (!is_IB) {
      *(int *)(gid.raw + 12) = inet_addr(hostname);
      debug_printf("UC WRITE from %s src ip addr %x%x %d.%d.%d.%d\n", hostname,
             gid.raw[10], gid.raw[11], gid.raw[12], gid.raw[13], gid.raw[14],
             gid.raw[15]);
    }
  }
  attr.path_mtu = IBV_MTU_4096;
  attr.rq_psn = 1234;

  debug_printf("UC dlid %d gid_index %d\n", dlid, gid_index);
  attr.ah_attr.dlid = is_IB ? dlid : 0;
  attr.ah_attr.sl = 0;
  attr.ah_attr.src_path_bits = 0;
  attr.ah_attr.port_num = 1;              // do not change
  attr.ah_attr.is_global = is_IB ? 0 : 1; // 1 below is valid

  attr.ah_attr.grh.hop_limit = 0xff;
  attr.ah_attr.grh.dgid = gid;
  attr.ah_attr.grh.sgid_index = gid_index; //<= see show_gids
  flag |= IBV_QP_STATE | IBV_QP_AV | IBV_QP_PATH_MTU | IBV_QP_DEST_QPN |
          IBV_QP_RQ_PSN;

  if (ibv_modify_qp(roce->qp[roi_id], &attr, flag)) {
    debug_printf("Failed to modify QP to RTR\n");
    release_ib_context(RELEASE_IBV_DESTROY_QP, roi_id);
    return RASHPA_LINK_ERROR;
  }

  debug_printf("ibvqprtr done for roi_id %d\n", roi_id);

  // steering rule for JF UDP
#if 0
#ifndef __PPC__
  if (service_type == IBV_QPT_RAW_PACKET && direction == RASHPA_BACKEND) {
    struct raw_eth_flow_attr1 {
      struct ibv_flow_attr attr;
      struct ibv_flow_spec_eth spec_eth;
    } __attribute__((packed)) flow_attr1 = {

        .attr =
            {
                .comp_mask = 0,
                .type = IBV_FLOW_ATTR_NORMAL,
                .size = sizeof(flow_attr1),
                .priority = 0,
                .num_of_specs = 1,
                .port = 1,
                .flags = 0,
            },

        .spec_eth = {.type = IBV_EXP_FLOW_SPEC_ETH,
                     .size = sizeof(struct ibv_flow_spec_eth),
                     .val =
                         {
                             .dst_mac = {0x50, 0x6b, 0x4b, 0xd3, 0xd1,
                                         0x85}, // DEST_MAC,
                             .src_mac = {0x50, 0x6b, 0x4b, 0xd3, 0xd1,
                                         0x64}, // srce_MAC,
                             .ether_type = 0,
                             .vlan_tag = 0,
                         },
                     .mask = {
                         .dst_mac = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
                         .src_mac = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
                         .ether_type = 0,
                         .vlan_tag = 0,
                     }}};

    struct ibv_flow *eth_flow;
    eth_flow = ibv_create_flow(roce->qp[roi_id], &flow_attr1.attr);
    if (!eth_flow) {
      fprintf(stderr, "Couldn't attach steering flow\n");
      exit(1);
    }
  }
  #endif
#endif
  return RASHPA_LINK_OK;
}

/**
 * init with parameters from RASPA telegram
 */

int rashpa_rocev2_uc_backend_receiver_init(int roi_id) {
  roce_ctx_t *roce = &g_roce;
  rashpa_ctx_t *link = &g_rashpa;
  int bunch = link->rashpa_bunch;
  // FIXME sizeof(float)
  int bdepth = link->bdepth;
  int gpu_w = link->gpu_w * bdepth;
  int gpu_h = link->gpu_h;
  int page_size = sysconf(_SC_PAGESIZE);

  if (roi_id == 0) {
    //
    create_ib_context();

    // register dest Memory Region
    debug_printf("ibv_reg_mr link->dest_addr %p for size %d\n", link->dest_addr,
                 2 * gpu_w * gpu_h * bunch);
    // x 2 gpu_h mandatory
    roce->mr =
        ibv_reg_mr(roce->pd, link->dest_addr, (size_t)gpu_w * 2 * gpu_h * bunch,
                   IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
    if (!roce->mr) {
      debug_printf(A_C_RED "Couldn't register MR: check nv_peer_mem, GPU PCIe "
                           "BAR limit (lspci -vv -s af:00.0)...\n" A_C_RESET);
      release_ib_context(RELEASE_IBV_DEALLOC_PD, 0);
      return RASHPA_LINK_ERROR;
    }

    roce->rkey[roi_id] = roce->mr->rkey;
    roce->daddr[roi_id] = roce->mr->addr;

    debug_printf("UC waiting for remote WRITE qp_num %d data at buffer addr %p  "
           "rkey %x mem sz %p\n",
           roce->remote_qpn[roi_id], roce->mr->addr, roce->mr->rkey,
           gpu_w * gpu_h * bunch);
  }
  // debug_printf("create_ib_cq_qprtr for roi_id %d\n",roi_id);
  create_ib_cq_qprtr(RASHPA_BACKEND, roi_id);

  return RASHPA_LINK_OK;
}

void *rashpa_rocev2_uc_backend_receiver_mainloop_single_thread(void *arg) {
  roce_ctx_t *roce = &g_roce;
  rashpa_ctx_t *link = &g_rashpa;
  int service_type = roce->service_type;
  int bunch = link->rashpa_bunch;
  int roi_number = link->roi_number;
  int nImages = link->nImages;
  int nIter = link->nIter;
  int bdepth = link->bdepth;
  int gpu_w = link->gpu_w * bdepth;
  int gpu_h = link->gpu_h;
  int *w = link->roi_w;
  int *h = link->roi_h;
  int *off_x = link->roi_dst_x;
  int *off_y = link->roi_dst_y;
  int page_size = sysconf(_SC_PAGESIZE);

  printf("rashpa_rocev2_uc_backend_receiver version %s %s\n",__DATE__,__TIME__);

  debug_printf("rashpa_rocev2_uc_backend_receiver_mainloop_single_thread "
         "service_type %d, bunch %d, roi_number %d,\n"
         "nImages %d, nIter %d, w[0] %d, h[0] %d, bdepth %d, gpu_w %d, gpu_h "
         "%d, off_x %d, off_y %d\n",
         service_type, bunch, roi_number, nImages, nIter, w[0], h[0], bdepth,
         gpu_w, gpu_h, off_x[0], off_y[0]);

  /**
   *
   * cpu affinity for efficient nic/gpu communication
   *
   */
  set_cpu(5);
 
 //prepare descriptors
  struct ibv_sge *sg;
  struct ibv_recv_wr *bad_wr;
  struct ibv_recv_wr *wr;
  struct ibv_wc *wc;
  int nWr;


//UC
if (service_type == IBV_QPT_UC) {
  nWr = bunch;

  sg = (struct ibv_sge *)memalign(page_size, nWr/2 * sizeof(struct ibv_sge));
  wr = (struct ibv_recv_wr *)memalign(page_size,
                                      nWr/2 * sizeof(struct ibv_recv_wr));
  wc = (struct ibv_wc *)memalign(page_size, 1 * sizeof(struct ibv_wc));

  memset(sg, 0, sizeof(struct ibv_sge));
  memset(wr, 0, 1 * sizeof(struct ibv_recv_wr));

  for (int i = 0; i < nWr / 2; i++) {
    sg[i].addr = NULL; // only interrested by IMM data
    sg[i].length = 0;
    sg[i].lkey = 0;
    wr[i].wr_id = 1234;
    wr[i].sg_list = &sg[i];
    wr[i].next = &wr[i + 1];
    wr[i].num_sge = 1;
  }
  wr[nWr / 2 - 1].next = NULL;
}
else {
//RAW for Jungfrau
  nWr = bunch*h[0];

  sg = (struct ibv_sge *)memalign(page_size, nWr/2 * sizeof(struct ibv_sge));
  wr = (struct ibv_recv_wr *)memalign(page_size,
                                      nWr/2 * sizeof(struct ibv_recv_wr));
  wc = (struct ibv_wc *)memalign(page_size, nWr * sizeof(struct ibv_wc));

  memset(sg, 0, sizeof(struct ibv_sge));
  memset(wr, 0, 1 * sizeof(struct ibv_recv_wr));

  for (int i = 0; i < nWr/2; i++) {
    sg[i].addr = link->dest_addr + i* w[0]*bdepth;
    sg[i].length = w[0]*bdepth;
    sg[i].lkey = roce->qkey;
    wr[i].wr_id = 1234;
    wr[i].sg_list = &sg[i];
    wr[i].next = &wr[i + 1];
    wr[i].num_sge = 1;
  }
  wr[nWr / 2 - 1].next = NULL;
}
  // Post Recv Work Request and wait for SoF
  int toofast = 0, missed = 0;
  int sig = 0;
  sig = 0;
  missed = 0;

  // polling bunch / 2 images
  int p, n;

  // GPUDirect change the order of K1K2 D1D2, make a first step now
  if (link->receiver_type == RASHPA_RECEIVER_NVIDIA_CUDA_GPU) {
    // debug_printf("pstatusFlag %p\n",pstatusFlag);
    *(volatile int *)pstatusFlag = KERNELREADY + sig++;
    usleep(5000);
  }

  // debug_printf("starting with nwr \n", nWr);
  gpustate = 1;

#if (SYNCHRO == 0)
  // initial post
  if (ibv_post_recv(roce->qp[0], wr, &bad_wr)) {
    printf("error ibv_post_recv 01\n");
    return -1;
  }
  do {
    if (ibv_post_recv(roce->qp[0], wr, &bad_wr)) {
      printf("error ibv_post_recv 11\n");
      return -1;
    }
    // printf("in loop polling for image %d\n",gpustate);

    n = 0;
    do {
      n = ibv_poll_cq(roce->cq[0], 1, wc);
    } while (n == 0);
    // printf("Waiting Start of Frame ... %d %d bunch %d\n",
    // 		ntohl(wc[0].imm_data),ntohl(wc[0].imm_data)%bunch,bunch);
  } while ((wc[0].wc_flags & IBV_WC_WITH_IMM) != IBV_WC_WITH_IMM ||
           (ntohl(wc[0].imm_data) % bunch) != bunch - 1);

  debug_printf("Got Start of Frame ... %d/%d %u nWr %d\n", wc[0].wc_flags,
         IBV_WC_WITH_IMM, ntohl(wc[0].imm_data), nWr);

#else // SYNCHRO==1 packets are time synced by source
  struct timespec t;

  printf("nothing done during main synchro\n");
  do {
    if (ibv_post_recv(roce->qp[0], wr, &bad_wr)) {
      printf("error ibv_post_recv 2 %d (too many  wr)\n", sig);
      goto _end;
    }

    n = 0;
    do {
      n = ibv_poll_cq(roce->cq[0], 1, wc);

    } while (n == 0);
  } while ((ntohl(wc[n - 1].imm_data) >> 24) != (NMODULES - 1));
#endif

  int nImg = 1, num, onum, bad_im = 0, cpy_error = 0;
  struct timespec startTime, endTime;

  clock_gettime(CLOCK_MONOTONIC, &startTime);

  /////////////////////
  // entering main loop until gpu compute completion
  // loop //	while (nImg < 2*nIter*nImages)

  int imm, oimm;
  while (gpustate) {
// printf("posting for an image %d service_type %d\n",nImg,service_type);

#ifdef MULTI_MODULES // ONLY for UC WRITE
    for (int nModule = 0; nModule < NMODULES; nModule++) {
#endif
      if (ibv_post_recv(roce->qp[0], wr, &bad_wr)) {
        printf("error ibv_post_recv 2 %d (too many  wr)\n", sig);
        goto _end;
      }

      p = 0;
      // npoll=nWr/2;
      do {
        n = ibv_poll_cq(roce->cq[0], 1, wc); // change to bunch/2-p for RAW packet
        if (n > 0) {
          oimm = imm;
          imm = ntohl(wc[n - 1].imm_data);
          if ((imm - oimm) != 1)
            printf("missed frame %d\n", ntohl(wc[n - 1].imm_data));
        }
        p += n;
      } while (p < (bunch / 2));
// printf("wc[n-1].src_qp %d\n",wc[n-1].src_qp);
// printf("module id %d frame
// %d\n",ntohl(wc[n-1].imm_data)>>24,ntohl(wc[n-1].imm_data)&0xFFFFFF);

#ifdef MULTI_MODULES
    }
#else // END MULTI

    if ((wc[0].wc_flags & IBV_WC_WITH_IMM) != IBV_WC_WITH_IMM) {
      // if (bad_im==0)
      printf(A_C_RED "#### IMM data not present\n" A_C_RESET);
      bad_im++;
    }

#endif

    // /* verifying the completion status just to be sure */
    // int status = 1;
    // for (int k=0; k<l;k++)
    // {
    // 	status = status && (wc[k].status == IBV_WC_SUCCESS);
    // }
    // if (!status)
    // {
    // 	debug_printf("Failed status near %s (%d) for wr_id %d nCompletion %d\n",
    // 		ibv_wc_status_str(wc[0].status),wc[0].status,
    // (int)wc[0].wr_id,nCompletion);
    // 	exit(0);
    // }

    if (sig * bunch == nIter * nImages)
      clock_gettime(CLOCK_MONOTONIC, &endTime);

    // check if cudamemcopy take too long ? status flag is reset after transfer
    // completion
    if ((link->receiver_type != RASHPA_RECEIVER_NVIDIA_CUDA_GPU) &&
        ((*(volatile int *)pstatusFlag) != -1)) {
      printf(
          A_C_RED
          "#### cudaMemcpy takes too long sig %d nImg %d flag %d\n" A_C_RESET,
          sig, nImg, *(volatile int *)pstatusFlag);
      cpy_error++;
    }

    // signal GPU after 1/2 x bunch  images
    // printf("signal %d %d %d\n",sig,nImg,link->nImgOnGpu);
    // this lock is polled by gpu accelerator using cuStreamWaitValue32
    *(volatile int *)pstatusFlag = KERNELREADY + sig++;

    // change bank
    nImg = (nImg == 0) ? 1 : 0;

    // check GPU compute completion
    if ((sig - 2 * link->nImgOnGpu) > 2) {
      printf(A_C_RED "#### GPU SLOW (sig %d image %d)\n" A_C_RESET, sig,
             2 * link->nImgOnGpu);
      toofast++;
    }
    //~ usleep(10000);
  }
_end:
  clock_gettime(CLOCK_MONOTONIC, &endTime);

  // purge NIC
  p = 0;
  do {
    n = ibv_poll_cq(roce->cq[0], nWr, wc);
    usleep(5);
    p++;
  } while (n > 0 || p < 40000);

  char msg[1024];
  sprintf(msg, "link=\'%s\' missed = \'%d\' toofast = \'%d\' bad_im = \'%d\' "
               "cpy_error = \'%d\'",
          RASHPA_LINK_NAME[link->link_type], missed, toofast, bad_im,
          cpy_error);

  // give some time finishing process the kernel & saving file. Print results
  usleep(5000);
  if (missed > 1 || toofast || bad_im || cpy_error > 0)
    printf(A_C_RED);
  else
    printf(A_C_GRN);
  displayProfilingResult(msg, &endTime, &startTime, w[0], 2 * h[0],nImages*nIter);
  printf(A_C_RESET);

  // cleanup
  link->romulu_state = ROMULU_LINK_READY;

  // free(sg);
  // free(wr);
  // free(wc);
  return NULL;
}

int rashpa_rocev2_uc_detector_init(int roi_id) {
  rashpa_ctx_t *link = &g_rashpa;
  roce_ctx_t *roce = &g_roce;
  void *src_addr = link->source_addr;
  uint64_t totalSize = link->dsSize;

  if (roi_id == 0) {
    create_ib_context();

    // Register source Memory Region
    debug_printf("ibv_reg_mr %p dsSize %ld\n", src_addr, totalSize);
    if (!src_addr) {
      printf("Couldn't allocate work buf.\n");
      release_ib_context(RELEASE_IBV_DESTROY_COMP_CHANNEL, roi_id);
      return RASHPA_LINK_ERROR;
    }
    roce->mr =
        ibv_reg_mr(roce->pd, src_addr, totalSize, IBV_ACCESS_LOCAL_WRITE);
    if (!roce->mr) {
      printf(A_C_RED "Couldn't register detector MR errno %d (12 check PERM) addr %p size "
                     "%ld\n" A_C_RESET,
             errno, src_addr, totalSize);
      release_ib_context(RELEASE_IBV_DEALLOC_PD, roi_id);
      return RASHPA_LINK_ERROR;
    }
  }
  // debug_printf("call create_ib_cq_qprtr roi_id %d\n",roi_id);
  if (create_ib_cq_qprtr(RASHPA_DETECTOR, roi_id) != RASHPA_LINK_OK)
    return RASHPA_LINK_ERROR;

  // Modify QP to RTS
  debug_printf("ibv_modify_qp to RTS\n");
  struct ibv_qp_attr attr;
  memset(&attr, 0, sizeof(attr));
  attr.qp_state = IBV_QPS_RTS;
  attr.sq_psn = 1234;
  if (ibv_modify_qp(roce->qp[roi_id], &attr, IBV_QP_STATE | IBV_QP_SQ_PSN)) {
    fprintf(stderr, "Failed to modify QP to RTS\n");
    release_ib_context(RELEASE_IBV_DESTROY_QP, roi_id);
    return RASHPA_LINK_ERROR;
  }

  /**
   * cpu affinity
   *
   */
  select_cpu(RASHPA_DETECTOR);

  return RASHPA_LINK_OK;
}

void *rashpa_rocev2_uc_detector_mainloop_single_thread(void *arg) {
  roce_ctx_t *roce = &g_roce;
  rashpa_ctx_t *link = &g_rashpa;

  int rkey = roce->rkey[0];
  int nImages = link->nImages;
  int bdepth = link->bdepth;
  // 					data set dimensions
  // int image_W = link->hsz * bdepth;
  // int image_H = link->vsz;
  // 					module or RoI size
  int *w = link->roi_w;
  int *h = link->roi_h;
  int *off_x = link->roi_src_x;
  int *off_y = link->roi_src_y;
  int bunch = link->rashpa_bunch;
  int nanodelay = link->nanodelay;

  int stride = link->gpu_stride;
  int gpu_h = link->gpu_h;
  int gpu_w = link->gpu_w;
  char *hostname = &link->hostname[0];
  int page_size = sysconf(_SC_PAGESIZE);
 
  int nWR = h[0];
  uint64_t pedestal;
  int imgCounter = 0;
  struct timespec startTime, endTime;
  int nImg = 0;

  printf("rashpa_rocev2_uc_detector version %s %s\n",__DATE__,__TIME__);

  debug_printf("rashpa_rocev2_uc_detector_mainloop_single_thread "
               "hostname %s,bdepth %d , w[0] %d, h[0] %d, "
               "off_x[0] %d, off_y[0] %d, \n"
               "gpu_h %d, stride %d, nWR %d,  rkey %x, \n"
               "bunch %d, nImages %d, bdepth %d, nanodelay %d\n"
               "roce->daddr[0] %p,link->source_addr %p\n",
               hostname, bdepth, w[0], h[0], off_x[0], off_y[0],
               gpu_h, stride, nWR, rkey, bunch, nImages, bdepth, nanodelay,
               roce->daddr[0], link->source_addr);

#if PERFORMANCETEST
  // nWr at maximum, nImages at mini
  nWR = 4 * h[0];
  nImages = 1;
#endif

  struct ibv_wc *wc =
      (struct ibv_wc *)memalign(page_size, nWR * sizeof(struct ibv_wc));
  struct ibv_sge *sg =
      (struct ibv_sge *)memalign(page_size, nWR * sizeof(struct ibv_sge));
  struct ibv_send_wr *wr = (struct ibv_send_wr *)memalign(
                         page_size, nWR * sizeof(struct ibv_send_wr)),
                     *bad_wr;

  memset(sg, 0, nWR * sizeof(struct ibv_sge));
  memset(wr, 0, nWR * sizeof(struct ibv_send_wr));

#define MAKE_DESC                                                              \
  wr[i].wr_id = nImg * h[0] + i;                                               \
  /* dst addr	*/                                                             \
  pedestal = (gpu_h == h[0])                                                   \
                 ? 0                                                           \
                 : (i < (nWR / 2)) ? 0 : (gpu_h * gpu_w * bdepth * 3 / 8);     \
  wr[i].wr.rdma.remote_addr =                                                  \
      ((uint64_t)roce->daddr[0]) + i * (w[0] * bdepth + stride) +              \
      (nImg % bunch) * gpu_h * gpu_w * bdepth + pedestal + off_x[0] * bdepth + \
      off_y[0] * gpu_w * bdepth;                                               \
  /* src addr */                                                               \
  sg[i].addr = (uint64_t)link->source_addr + (nImg * h[0] + i) * w[0] * bdepth;

  for (int i = 0; i < nWR; i++) //  nWr = h
  {
    wr[i].wr_id = i;
    wr[i].opcode = IBV_WR_RDMA_WRITE;
    wr[i].wr.rdma.rkey = rkey;
    wr[i].next = &wr[i + 1];
    wr[i].sg_list = &sg[i];
    wr[i].num_sge = 1;
    sg[i].length = w[0] * bdepth;
    sg[i].lkey = roce->mr->lkey;

    MAKE_DESC
  }
  wr[h[0] - 1].next = NULL;
  wr[h[0] - 1].opcode = IBV_WR_RDMA_WRITE_WITH_IMM;
  wr[h[0] - 1].send_flags = IBV_SEND_SIGNALED;

// init

#if SYNCHRO == 1
  struct timespec t, t2;
  // main synchro
  // do
  // {
  // 	clock_gettime(CLOCK_MONOTONIC, &t);
  // }
  // while (t.tv_nsec  >100 && (t.tv_sec%2) );

  int sock, sock2, r;
  struct sockaddr_in adr, adr2;
  sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock == -1)
    printf("erreur socket\n");
  sock2 = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock2 == -1)
    printf("erreur socket\n");

  adr.sin_family = AF_INET;
  adr.sin_port = htons(4440 + nanodelay);
  adr.sin_addr.s_addr = INADDR_ANY;

  adr2.sin_family = AF_INET;
  adr2.sin_port = htons(4440 + ((nanodelay + 1) % NMODULES));

  // FIXME HARD CODE next module address
  adr2.sin_addr.s_addr = inet_addr(hostname);

  // rashpa manager ip addr and port
  r = bind(sock, (struct sockaddr *)(&adr), sizeof(struct sockaddr));
  char data[4];

#endif

  clock_gettime(CLOCK_MONOTONIC, &startTime);

#if SYNCHRO == 0
  // a first batch of WR to half fill HCA
  // first post
  wr[h[0] - 1].imm_data = htonl(imgCounter++);
  if (ibv_post_send(roce->qp[0], wr, &bad_wr)) {
    fprintf(stderr, "Error, ibv_post_send(1) failed\n");
    return NULL;
  }
#endif

  /**
   * detector main loop
   *
   * sending image data line by line
   * RASHPA event at the end of each Frame by IMM data
   *
   * - multi module externaly sync by manager (UDP datagram)
   * - delay issue due to asynchronicity
   */
  uint64_t raddr, oraddr;
  while (link->remu_state == REMU_RUNNING) {
    nImg = imgCounter % nImages;
// debug_printf("detector main loop %d counter %d  %d interimg %d\n",
//                 nImg,imgCounter,nImg%bunch,gpu_h*gpu_w*bdepth);

#if SYNCHRO == 1
    // Wait for RASHPA EVENT
    recvfrom(sock, data, 4, 0, NULL, NULL);
    // printf("loop module %d/%d    %d\n",nanodelay,nImg % bunch,*(int*)data);
    wr[nImg * h[0]].imm_data =
        htonl((imgCounter++ & 0xFFFFFF) | (nanodelay << 24));
#else
    wr[h[0] - 1].imm_data = htonl(imgCounter++);
    for (int i = 0; i < nWR; i++) {
      MAKE_DESC
    }
    oraddr = raddr;
    raddr = wr[0].wr.rdma.remote_addr;

// CHECKME
// if (raddr-oraddr != gpu_h*gpu_w*bdepth)
// 	printfraddr-("raddr-oraddr != nImg %d\n",nImg%bunch);

#endif
    if (ibv_post_send(roce->qp[0], wr, &bad_wr)) {
      fprintf(stderr, "Error, ibv_post_send(2) failed at %d\n", nImg);
      return NULL;
    } 

    int l, num_comp;
    num_comp = 0;
    // debug_printf("polling\n");
    do {
      num_comp = ibv_poll_cq(roce->cq[0], 1, wc);
    } while (num_comp == 0);
    // debug_printf("done %d %d\n",num_comp,l);

    /* verify the completion status just to be sure */
    // int status = 1;
    // for (int i=0; i<l; i++)
    // {
    // 	status = status && (wc[i].status == IBV_WC_SUCCESS);
    // }
    if (wc[0].status != IBV_WC_SUCCESS) {
      debug_printf("%s for wr_id %d\n", ibv_wc_status_str(wc[0].status),
                   (int)wc[0].wr_id);
      exit(0);
    }

    if (nImg == nImages - 1) {
      clock_gettime(CLOCK_MONOTONIC, &endTime);
      //CHECKME
      double dt_ms = (endTime.tv_nsec - startTime.tv_nsec) / 1000000.0 +
                     (endTime.tv_sec - startTime.tv_sec) * 1000.0;
      double gbps =
          (double)nImages  * w[0] * h[0] * bdepth * 8 / dt_ms * 1e3 / 1e9;
#if SYNCHRO == 0
      printf("BW \t\t%01f Gbits/s\r", gbps);
      fflush(stdout);
#endif
      clock_gettime(CLOCK_MONOTONIC, &startTime);
    }
#if SYNCHRO == 0
//checkme : asynchronous way to slowdown, bad idea here....
    if (nanodelay)
      usleep(nanodelay);
#else
    // send event to the adjacent detector module (token ring)
    sendto(sock2, data, 4, 0, (struct sockaddr *)(&adr2),
           sizeof(struct sockaddr_in));
#endif
    if (imgCounter == -1) {
      printf("Running for too long time. Exiting....\n");
      exit(0);
    }
  } // detector main loop

  free(sg);
  free(wr);
  free(wc);
  return NULL;
}
