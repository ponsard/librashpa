/*
 *
 * UDP ZMQ
 *
 *
 *
 *
 */

#define _GNU_SOURCE
#include <arpa/inet.h>
#include <assert.h>
#include <fcntl.h>
#include <malloc.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sched.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include <infiniband/verbs.h>
#include <numa.h>

#include "librashpa.h"
#include "rashpa_init.h"
#include "rashpa_main.h"
#include "rashpa_roce_uc.h"
#include "rashpa_udp.h"
#include "rashpa_zmq.h"

#include "../../../romulu/controller/src/romulu_jf.h"

extern udp_ctx_t g_udp;

volatile int *pstatusFlag,*pstoreNUM;
volatile int statusFlag,storeNUM;

// int counter = 0;

void rashpa_fini(int dir)
{
  rashpa_ctx_t *link = &g_rashpa;

  switch (link->link_type)
  {

  case RASHPA_LINK_ROCEV2_RAW:
  case RASHPA_LINK_ROCEV2_UC:
    release_ib_context(RELEASE_IBV_POLL, 0);
    break;

  case RASHPA_LINK_ZMQ:
    rashpa_zmq_fini(dir);
    break;

  case RASHPA_LINK_UDP:
  case RASHPA_LINK_UDP_JF:
    rashpa_udp_fini(dir);
    break;
  default:
    printf("link not yet implemented\n");
  }
  printf("end of rashpa_fini\n");
}

int rashpa_init(int dir)
{
  rashpa_ctx_t *link = &g_rashpa;

  switch (link->link_type)
  {

  case RASHPA_LINK_ROCEV2_RAW:
  case RASHPA_LINK_ROCEV2_UC:
    create_ib_context(dir);
    if (dir == RASHPA_DETECTOR)
      return (rashpa_rocev2_uc_detector_init(0));
    if (dir == RASHPA_BACKEND)
      return (rashpa_rocev2_uc_backend_receiver_init(0));
    break;

  case RASHPA_LINK_ZMQ:
    rashpa_recv_line = rashpa_zmq_recv_line;
    rashpa_recv_lines = rashpa_zmq_recv_lines;
    rashpa_send_line = rashpa_zmq_send_line;
    rashpa_buffer_init = rashpa_zmq_buffer_init;
    return (rashpa_zmq_init(dir));
    break;

  case RASHPA_LINK_UDP:
  case RASHPA_LINK_UDP_JF:
    rashpa_recv_line = rashpa_udp_recv_line;
    rashpa_recv_lines = rashpa_udp_recv_lines;
    rashpa_send_line = rashpa_udp_send_line;
    rashpa_buffer_init = rashpa_udp_buffer_init;
    return (rashpa_udp_ini(dir));
    break;

  default:
    printf("link not yet implemented\n");
  }
  return RASHPA_LINK_ERROR;
}

void select_cpu(int dir)
{
  cpu_set_t set;
  CPU_ZERO(&set);
  if (dir == RASHPA_BACKEND) // scisoft14
  {
    CPU_SET(1, &set);
    CPU_SET(3, &set);
    CPU_SET(5, &set);
    CPU_SET(7, &set);
  }
  else if (dir == RASHPA_BACKENDGPU) // scisoft14
  {

    CPU_SET(9, &set);
    CPU_SET(11, &set);
    CPU_SET(13, &set);
    CPU_SET(15, &set);
  }
  else if (dir == RASHPA_DETECTOR) // scisoft13
  {
    CPU_SET(1, &set);
    CPU_SET(3, &set);
    CPU_SET(5, &set);
    CPU_SET(7, &set);
    CPU_SET(9, &set);
    CPU_SET(11, &set);
  }
  else
  {
    debug_printf("select_cpu error\n");
    return;
  }
  sched_setaffinity(0, sizeof(set), &set);
  unsigned cpu = sched_getcpu();
  pthread_t self = pthread_self();
  pthread_setaffinity_np(self, sizeof(cpu_set_t), &set);
  debug_printf(A_C_BLU "thread %s using cpu %d\n" A_C_RESET,
               (dir == RASHPA_BACKEND) ? "RASHPA_BACKEND" : "RASHPA_DETECTOR",
               cpu);
}
void set_cpu(int n)
{
  cpu_set_t set;
  CPU_ZERO(&set);
  CPU_SET(n, &set);
  sched_setaffinity(0, sizeof(set), &set);
  unsigned cpu = sched_getcpu();
  pthread_t self = pthread_self();
  pthread_setaffinity_np(self, sizeof(cpu_set_t), &set);
  debug_printf(A_C_BLU "thread using cpu %d\n" A_C_RESET, cpu);
}

void *rashpa_receiver_mainloop(void *arg)
{
  // select_cpu(RASHPA_BACKEND);
  set_cpu(15);
  pstatusFlag = &statusFlag;
  rashpa_ctx_t *link = &g_rashpa;
  int roi_number = link->roi_number;
  int nImages = link->nImages;
  int nIter = link->nIter;
  int gpucode = link->gpucode;
  int bdepth = (gpucode == 2) ? 1 : link->bdepth;
  int gpu_w = link->gpu_w * bdepth;
  int bunch = link->rashpa_bunch;
  // int 				batch				=
  // link->batch;
  int page_size = sysconf(_SC_PAGESIZE);

  int *w = (int *)memalign(page_size, roi_number * sizeof(int));
  int *h = (int *)memalign(page_size, roi_number * sizeof(int));
  int *off_x = (int *)memalign(page_size, roi_number * sizeof(int));
  int *off_y = (int *)memalign(page_size, roi_number * sizeof(int));

  for (int i = 0; i < roi_number; i++)
  {
    w[i] = link->roi_w[i] * bdepth;
    h[i] = link->roi_h[i];
    off_x[i] = link->roi_dst_x[i] * bdepth;
    off_y[i] = link->roi_dst_y[i];
    printf("roi %d w[i] %d h[i] %d off_x[i] %d, off_y[i] %d\n", i, w[i], h[i],
           off_x[i], off_y[i]);
  }
  // link->dest_addr = numa_alloc_local(h[0]*bunch*w[0]*2);
  // link->dest_addr = malloc(h[0]*bunch*w[0]*2);
  printf("dest_addr %p\n", link->dest_addr);

  // precompute descriptors CHECKME not sure it improves performances
  void **lineAddr = memalign(page_size, bunch * h[0] * sizeof(void *));
  for (int i = 0; i < h[0] * bunch; i++)
  {
    // let compute where are the ROI bytes of line i
    lineAddr[i] = (void *)(((uint64_t)link->dest_addr) + off_y[0] * gpu_w +
                           off_x[0] + i * w[0]);
    // printf("lines %d lineAddr %p\n",lines, lineAddr[lines]);
  }

  struct timespec startTime, endTime;

  int nImg = 1, error = 0, toofast = 0;

  link->romulu_state = ROMULU_RUNNING;
  printf("purge NIC and warmup... (VMA requires longer warmup)\n");

  gpustate = 1;
  int sig = 0;
  int num = 0, onum = 0;

  // VERY large number required to purge the NIC

  int nWarmup = 1 << 16; // w/o VMA;
  // int nWarmup = 1<<24; //VMA in use;

  if (link->link_type == RASHPA_LINK_ZMQ)
    nWarmup = 300;

  for (int i = 0; i < nWarmup; i++)
  {
    rashpa_recv_line(lineAddr[0], w[0]);
    // printf ("purge NIC and warmup %d...\n",i);
  }

  printf("warmup done. (delay required). Waiting for Sof\n");

  int roi_id = 0;
  // wait for SOF depends on Detector frame format
  while (1)
  {
    // printf ("wait SOF\n");
    rashpa_recv_line(lineAddr[0], w[0]); // h[0]/2*
    // printf ("SoF %d\n",((*(volatile unsigned int*)(lineAddr[0])) % (2*h[0]))
    // );
    if ((gpucode != 2) && // SoF in 4 first bytes data
        (((*(volatile unsigned int *)(lineAddr[0])) % (2 * h[0])) ==
         (2 * h[0] - 1)))
      break;

    // SoF for JungFrau, must be on last line of a a pedestal
    if ((gpucode == 2) &&                                  // Jungfrau detector
        ((*(((char *)lineAddr[0]) + 18)) & 0xff) == 127 && // line number 127
        ((*(((char *)lineAddr[0]) + 6)) & 0xff) % 2 == 1   // image num odd
    )
      break;

    // printf ("searching SOF nIter %d, roi_number %x%x%x\n",nIter ,
    // *(char*)(addr+0) &0xff,*(char*)(addr+1) &0xff,*(char*)(addr+2)  &0xff);
  }
  volatile int tag0 = *(volatile unsigned int *)(lineAddr[0]), tag1, tag, diff;
  printf("got SOF tag %u / %d   ww %d\n", tag0, tag0 % h[0], w[0]);
  clock_gettime(CLOCK_MONOTONIC, &startTime);

  // start acquisition : nothing special to do for UDP/IP
  nImg = 0;

  udp_ctx_t *udp = &g_udp;

  while (gpustate)
  {
    // read a full image
    // printf ("nImg%d %d\n",nImg,h[0]);
    for (int i = nImg * h[0]; i < (nImg + 1) * h[0]; i++)
    {
      rashpa_recv_line(lineAddr[i], w[0]);
      // rashpa_recv_line(lineAddr[0],w[0]);

      tag = tag1;
      tag1 = *(volatile unsigned int *)(lineAddr[i]);
      // tag1 = *(volatile unsigned int*)(lineAddr[0]);
      // printf("tag %d\n",tag1);
      diff = tag1 - tag;

      if ((diff != 1)) //&& (sig > 0))
      {
        printf("error at tag %d of %d\n", tag1 - tag0, diff);
        // attempt to mitigate the issue
        // i += diff-1;
        // if (i==h[0])
        // {
        // 	i -= h[0];
        // 	nImg++;
        // 	if (nImg==bunch)
        // 		nImg=0;
        // }
        error++;
      }
    }

    // signal to GPU that data are ready
    if ((nImg % (bunch / 2)) == ((bunch / 2) - 1))
    {
      // missing packet detection
      // if (gpucode!=2)
      // {
      // 	num = *(volatile unsigned int*)(lineAddr[(nImg+1)*h[0]-1]);
      // 	// printf ("checking tag %u / %d\n",num, num%h[0] );
      // 	if (num%h[0] != (h[0]-1))
      // 	{
      // 		error ++ ;
      // 		printf ("error in img %d... \n",sig);
      // 	}
      // }
      // else
      // {
      // 	if (((*(((char*)lineAddr[(nImg+1)*h[0]-1])+18)) & 0xff)     !=
      // 127 ||     //line number 127
      // 	    ((*(((char*)lineAddr[(nImg+1)*h[0]-1])+ 6)) & 0xff) % 2 !=
      // 0)
      // 	{
      // 		error ++ ;
      // 		printf ("error in jf img %d...
      // \n",(*(((char*)lineAddr[0])+18)) & 0xff);
      // 	}
      // }

      // printf("signal %d nImg %d tag %u lost images %d\n",sig,nImg,tag1,
      // (num-tag1)-nIter*nImages*h[0]);
      *pstatusFlag = KERNELREADY + sig++;
    }

    nImg++;
    if (nImg >= bunch)
      nImg = 0;

    if ((sig - 2 * link->nImgOnGpu) > 2)
    {
      printf(A_C_RED
             "#### GPU SLOW (reseting sig %d image %d) ####\n" A_C_RESET,
             sig, 2 * link->nImgOnGpu);
      toofast++;
    }
    // usleep();
  }

  clock_gettime(CLOCK_MONOTONIC, &endTime);

  char msg[1024];
  if ((error > 1) ||
      toofast) //(link->nImgOnGpu * rashpa_bunch < nIter * nImages) ||
  {
    debug_printf(A_C_RED "missed %d toofast %d\n" A_C_RESET, error, toofast);
  }

  sprintf(msg, "link=\'%s\' missed = \'%d\' toofast = \'%d\'",
          RASHPA_LINK_NAME[link->link_type], error, toofast);

  if (error > 1 || toofast)
    printf(A_C_RED);
  else
    printf(A_C_GRN);
  if (gpucode == 2)
    displayProfilingResult(msg, &endTime, &startTime, 512, 1024, 4);
  else
    displayProfilingResult(msg, &endTime, &startTime, w[0], h[0], nImages);

  printf(A_C_RESET);

  link->romulu_state = ROMULU_LINK_READY;
  return NULL;
}

void *rashpa_detector_mainloop(void *arg)
{
  select_cpu(RASHPA_DETECTOR);

  rashpa_ctx_t *link = &g_rashpa;
  int roi_number = link->roi_number;
  int bdepth = link->bdepth;
  int nImages = link->nImages;
  int image_W = link->hsz * bdepth;
  int image_H = link->vsz;
  int page_size = sysconf(_SC_PAGESIZE);
  int *w = (int *)memalign(page_size, roi_number * sizeof(int));
  int *h = (int *)memalign(page_size, roi_number * sizeof(int));
  int *off_x = (int *)memalign(page_size, roi_number * sizeof(int));
  int *off_y = (int *)memalign(page_size, roi_number * sizeof(int));

  for (int i = 0; i < roi_number; i++)
  {
    w[i] = link->roi_w[i] * bdepth;
    h[i] = link->roi_h[i];
    off_x[i] = link->roi_src_x[i] * bdepth;
    off_y[i] = link->roi_src_y[i];
    printf("roi %d w[i] %d h[i] %d off_x[i] %d, off_y[i] %d delay %d nImages "
           "%d link->source_addr %p\n",
           i, w[i], h[i], off_x[i], off_y[i], link->nanodelay, nImages,
           link->source_addr);
  }

  // consider warmup
  int nImg;
  void *data;
  unsigned int counter = 0;
  // precompute image line descriptors
  void **lineAddr = memalign(page_size, nImages * h[0] * sizeof(void *));
  for (int nImg = 0; nImg < nImages; nImg++)
    for (int i = nImg * h[0]; i < (nImg + 1) * h[0]; i++)
    {
      // let compute where are the ROI bytes of line i
      lineAddr[i] =
          ((uint64_t)link->source_addr) // location of dataset
          //+ (nImg % nImages)*image_W*image_H		//where start next image
          + off_y[0] * image_W // where top of roi start
          + i * w[0]           // add i lines
          + off_x[0];
    }

  // detector main loop
  struct timespec startTime, endTime;
  clock_gettime(CLOCK_MONOTONIC, &startTime);

  // start sending 1 x image
  // we mimic what is done for RoCE transfer
  // *(volatile int*)lineAddr[0] = counter;
  // counter += h[0];
  // for (int i = 0; i < h[0]; i++)
  // {
  // 	*(volatile int*)lineAddr[i] = counter++;
  // 	rashpa_send_line(lineAddr[i], w[0]);
  // }

  nImg = 0;

  while (link->remu_state == REMU_RUNNING)
  {
    // *(volatile int*)lineAddr[nImg*h[0]] = counter;
    // counter += h[0];
    for (int i = nImg * h[0]; i < (nImg + 1) * h[0]; i++)
    {
      *(volatile unsigned int *)lineAddr[i] = counter++;
      rashpa_send_line(lineAddr[i], w[0]);
      // printf("rashpa_send_line %d\n",counter);
      // up to 50Gb/s when using small number of descriptors
    }

    nImg++;

    if (nImg >= nImages)
    {
      clock_gettime(CLOCK_MONOTONIC, &endTime);
      // double dt_ms = (endTime.tv_nsec - startTime.tv_nsec) / 1000000.0 +
      //                (endTime.tv_sec - startTime.tv_sec) * 1000.0;
      // double gbps = (double)nImages * h[0] * w[0] * 8 / dt_ms * 1e3 / 1e9;
      // printf("BW \t\t%01f Gb/s (%d %d %d %d)\r", gbps, nImages, h[0], w[0],
      //        nImg);
      fflush(stdout);
      clock_gettime(CLOCK_MONOTONIC, &startTime);
      nImg = 0;
    }
    if (counter == -1)
    {
      printf("Running for too long time. Exiting....\n");
      exit(0);
    }
  }

  return NULL;
}
