

#include "rashpa_init.h"
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

#include "librashpa.h"
#include "rashpa_main.h"

const char *ROMULU_RECEIVER[] = {"RASHPA_RECEIVER_NVIDIA_CUDA_GPU",
                                 "RASHPA_RECEIVER_AMD_GPU",
                                 "RASHPA_RECEIVER_OPENCL_XEON",
                                 "RASHPA_RECEIVER_NVIDIA_OPENCL_GPU",
                                 "RASHPA_RECEIVER_NVIDIA_CUDAMEM_GPU",
                                 "RASHPA_RECEIVER_NOGPU",
                                 NULL};

const char *RASHPA_LINK_NAME[] = {
    "RASHPA_LINK_NONE",      "RASHPA_LINK_GDRCOPY",
    "RASHPA_LINK_RSOCKET",   "RASHPA_LINK_ROCEV2_RAW",
    "RASHPA_LINK_ROCEV2_UC", "RASHPA_LINK_MEMCOPY",
    "RASHPA_LINK_ZMQ",       "RASHPA_LINK_UDP",
    "RASHPA_LINK_UDP_JF",    NULL};

const char *RASHPA_XML_ERRORS[] = {"RASHPA_XML_ERROR", "RASHPA_XML_BAD_LINK",
                                   "RASHPA_XML_BAD_RECEIVER", "RASHPA_XML_OK",
                                   NULL};

const char *REMU_STATE[] = {"REMU_IDLE", "REMU_SOURCE_CONFIGURED",
                            "REMU_LINK_READY", "REMU_RUNNING", NULL};

const char *ROMULU_STATE[] = {"ROMULU_IDLE",        "ROMULU_RASHPA_CONFIGURED",
                              "ROMULU_LINK_READY ", "ROMULU_GPU_READY",
                              "ROMULU_RUNNING",     NULL};

void rashpa_startup() {}

#ifndef __PPC__
unsigned long long rdtsc(void) {
  unsigned int low, high;
  asm volatile("rdtsc" : "=a"(low), "=d"(high));
  return low | ((unsigned long long)high) << 32;
}
#endif
static long diff_in_ns(struct timespec t1, struct timespec t2)
{
    struct timespec diff;
    if (t2.tv_nsec - t1.tv_nsec < 0)
    {
        diff.tv_sec = t2.tv_sec - t1.tv_sec - 1;
        diff.tv_nsec = t2.tv_nsec - t1.tv_nsec + 1000000000;
    }
    else
    {
        diff.tv_sec = t2.tv_sec - t1.tv_sec;
        diff.tv_nsec = t2.tv_nsec - t1.tv_nsec;
    }
    return (diff.tv_sec * 1000000000.0 + diff.tv_nsec);
}

void saveDataInFile(const char *fname, void *buffer, size_t len) {
  int f = open(fname, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU);
  if (f == -1) {
    printf("error open pinnedres?.data\n");
  }

  size_t res = 0;
  do {
    res += write(f, buffer + res, len - res);
    debug_printf("saving %ld bytes in %s\n", res, fname);
  } while (res < len);

  close(f);
}

void displayProfilingResult(const char *msg, 
                            struct timespec *end,struct timespec *start, 
                            int w, int h, int n) {
  rashpa_ctx_t *link = &g_rashpa;

  int bdepth = link->bdepth;
  int nImages = link->nImages;
  int nIter = link->nIter;
  int nImgProcessed = link->rashpa_bunch * link->batch * link->nImgOnGpu;

  double dt_ms = diff_in_ns(*start, *end) / 1000000.0;
  double gbps = ((double) w) * h * n * 8 / dt_ms * 1e3 / 1e9;
  double pps = ((double)h) * n / dt_ms;
  double fps = ((double)n) * 1000 / dt_ms;
  printf("%d nImages (%d x %d bytes) in %f ms\n", n, w, h, dt_ms);
  printf("%s %f Gbits/s, %.1f fps using single GPU core.\n" A_C_RESET, 
        msg, gbps, fps);
  
  sprintf(link->messages, "<rashpa_transfer %s  dt_ms=\' %f \' />", msg, dt_ms);
}

volatile int statusFlag;

