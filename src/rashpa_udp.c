#define _GNU_SOURCE /* See feature_test_macros(7) */

#include "rashpa_udp.h"
#include "librashpa.h"
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

udp_ctx_t g_udp;
struct mmsghdr msgs[VLEN];
struct iovec iovecs[VLEN];
struct timespec delay, rem;

// static void* recvBuf;
static char ibuf[9200];

int rashpa_udp_ini(int p) {

  rashpa_ctx_t *link = &g_rashpa;
  udp_ctx_t *udp = &g_udp;

  udp->s = socket(AF_INET, SOCK_DGRAM, 0);
  if (udp->s == -1) {
    printf("error creating UDP socket\n");
    return RASHPA_LINK_ERROR;
  }

  if (p == RASHPA_BACKEND) {
    printf("creating backend UDP server socket on port %d\n", link->port);

    // int disable = 1;
    // if (setsockopt(udp->s, SOL_SOCKET, SO_NO_CHECK, (void*)&disable,
    // sizeof(disable)) < 0) {
    //     perror("setsockopt failed");
    // }
    int value = 2 * 1024 * 1024 * 1024UL - 1;
    if (setsockopt(udp->s, SOL_SOCKET, SO_RCVBUF, &value, sizeof(value)) < 0) {
      perror("setsockopt failed");
    }
    int optval = 1;
    setsockopt(udp->s, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval));

    optval = 1;
    setsockopt(udp->s, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
    udp->dst_adr.sin_family = AF_INET;
    udp->dst_adr.sin_port =
        htons(link->port); /// htons(3333);//CHECKME htons(link->port);
    udp->dst_adr.sin_addr.s_addr = INADDR_ANY;

    if (bind(udp->s, (struct sockaddr *)(&udp->dst_adr),
             sizeof(struct sockaddr)) == -1) {
      printf("bind udp->s error\n");
      return RASHPA_LINK_ERROR;
    }

    // memset(msgs, 0, sizeof(msgs));
    // for (int i = 0; i < VLEN; i++) {
    //    msgs[i].msg_hdr.msg_iov    = &iovecs[i];
    //    msgs[i].msg_hdr.msg_iovlen = 1;
    // }
  }

  if (p == RASHPA_DETECTOR) {
    printf("creating detector UDP client socket on port %d to %s\n", link->port,
           link->hostname);

    udp->dst_adr.sin_family = AF_INET;
    udp->dst_adr.sin_port = htons(link->port);
    udp->dst_adr.sin_addr.s_addr = inet_addr(link->hostname);

    int value = 2 * 1024 * 1024 * 1024UL - 1;
    if (setsockopt(udp->s, SOL_SOCKET, SO_SNDBUF, &value, sizeof(value)) < 0) {
      perror("setsockopt failed");
    }
    // int optval = 1;
    // setsockopt(udp->s, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval));
    delay.tv_nsec = 1;
  }
  // else
  // {
  // 	int	page_size = sysconf(_SC_PAGESIZE);
  // 	recvBuf = memalign(page_size, 9192);
  // 	printf("udp_ini addr %p\n",recvBuf);

  // 	//recvBuf = malloc(9192);
  // }

  return RASHPA_LINK_OK;
}

void rashpa_udp_fini() {
  udp_ctx_t *udp = &g_udp;
  printf("close socket\n");
  // shutdown(udp->s,SHUT_RDWR);
  close(udp->s);
}

inline void rashpa_udp_send_line(void *buffer, int width) {
  rashpa_ctx_t *link = &g_rashpa;
  udp_ctx_t *udp = &g_udp;
  struct sockaddr_in dst_adr = udp->dst_adr;

  sendto(udp->s, buffer, width, 0, (struct sockaddr *)&dst_adr,
         sizeof(struct sockaddr_in));

  // dropped packet over 10Gb/s if no delay
 #ifndef __PPC__
  unsigned long long start = rdtsc(), now;
  if (link->nanodelay)
    do {
      now = rdtsc();
    } while ((now - start) < link->nanodelay);
    #endif
}

inline void rashpa_udp_recv_line(void *buffer, int width) {
  udp_ctx_t *udp = &g_udp;
  int err;

  if ((err = recvfrom(udp->s, buffer, width, 0, NULL, NULL)) != width)
  // if ((err=recvfrom(udp->s, ibuf, width, 0, NULL, NULL)) != width)
  {
    printf("recvfrom error %p w %d error %d\n", ibuf, width, err);
  };
  // it appears to be faster to do this than recvfrom at seraval addresses
  // memcpy(buffer, ibuf, width);
}

inline int rashpa_udp_recv_lines(void *buffer, int width, int n) {
  udp_ctx_t *udp = &g_udp;

  return recvmmsg(udp->s, msgs, n, 0, NULL);
}

inline void rashpa_udp_buffer_init(void *addr, int w) {

  for (int i = 0; i < VLEN; i++) {
    iovecs[i].iov_len = w;
    iovecs[i].iov_base = addr + i * w;
  }
}
