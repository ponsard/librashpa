/*
 * rashpa_xml.c
 *
 * Copyright 2018  <ponsard@ldel04>
 *
 *
 *
 *
 */

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <stdint.h>
#include <string.h>

#include "librashpa.h"
#include "rashpa_init.h"
//#include <infiniband/verbs.h>
#include "rashpa_roce_uc.h"

char xpath[256], *search, *xml_request,

    // datachannel
    xp_rashpa_link_data_source[] = "//rashpa_link/@data_source",
    xp_rashpa_link_type[] = "//rashpa_link/@link_type",

    xp_rashpa_link_nic[] = "//rashpa_link/@nic",
    // CHECKME add hostname number support, only 0 for now
    xp_rashpa_link_hostname[] = "//rashpa_link/@hostname",
    xp_rashpa_link_port[] = "//rashpa_link/@port",
    xp_rashpa_link_computeProcessPid[] = "//rashpa_link/@computeProcessPid",
    xp_rashpa_link_udsendSize[] = "//rashpa_link/@udsendSize",

    xp_rashpa_link_qpn[] = "//rashpa_link/@qpn",
    xp_rashpa_link_gid_index[] = "//rashpa_link/@gid_index",
    xp_rashpa_link_dlid[] = "//rashpa_link/@dlid",
    xp_rashpa_link_is_ib[] = "//rashpa_link/@is_ib",
    xp_rashpa_link_service_type[] = "//rashpa_link/@service_type",
    xp_rashpa_link_qkey[] = "//rashpa_link/@qkey",
    xp_rashpa_link_rkey[] = "//rashpa_link/@rkey",
    xp_rashpa_link_daddr[] = "//rashpa_link/@daddr",
    xp_rashpa_link_nanodelay[] = "//rashpa_link/@nanodelay",

    xp_rashpa_link_dest_addr[] = "//rashpa_link/@dest_addr",
    xp_rashpa_link_source_addr[] = "//rashpa_link/@device_source",
    xp_rashpa_link_rashpa_bunch[] = "//rashpa_link/@rashpa_bunch",

    // buffer
    xp_rashpa_backend_receiver_type[] = "//rashpa_backend/@receiver_type",
    xp_rashpa_backend_gpuId[] = "//rashpa_backend/@gpuId",
    xp_rashpa_backend_nImages[] = "//rashpa_backend/@nImages",
    xp_rashpa_backend_imgSize[] = "//rashpa_backend/@imgSize",
    xp_rashpa_backend_dsSize[] = "//rashpa_backend/@dsSize",
    xp_rashpa_backend_savePinnedBuf[] = "//rashpa_backend/@savePinnedBuf",
    xp_rashpa_backend_nIter[] = "//rashpa_backend/@nIter",
    xp_rashpa_backend_batch[] = "//rashpa_backend/@batch",
    xp_rashpa_backend_gpu_w[] = "//rashpa_backend/@gpu_w",
    xp_rashpa_backend_gpu_h[] = "//rashpa_backend/@gpu_h",
    xp_rashpa_gpu_stride[] = "//rashpa_backend/@gpu_stride",

    xp_rashpa_backend_bdepth[] = "//rashpa_backend/@bdepth",
    xp_rashpa_backend_gpucode[] = "//rashpa_backend/@gpucode",
    xp_rashpa_backend_gpustream[] = "//rashpa_backend/@gpustream",

    xp_rashpa_roi_id[] = "//rashpa_roi/@id",
    xp_rashpa_roi_stride[] = "//rashpa_roi/@roi_stride",
    xp_rashpa_roi_src_x[] = "//rashpa_roi/@src_x",
    xp_rashpa_roi_src_y[] = "//rashpa_roi/@src_y",
    xp_rashpa_roi_dst_x[] = "//rashpa_roi/@dst_x",
    xp_rashpa_roi_dst_y[] = "//rashpa_roi/@dst_y",
    xp_rashpa_roi_w[] = "//rashpa_roi/@w",
    xp_rashpa_roi_h[] = "//rashpa_roi/@h",
    xp_rashpa_roi_gain_file[] = "//rashpa_roi/@gain_file";

// xp_rashpa_veto_h[]						=
// "//rashpa_roi/@veto_h",
// xp_rashpa_veto_l[]						=
// "//rashpa_roi/@veto_l";

xmlXPathContextPtr context;
xmlDocPtr doc;
xmlXPathObjectPtr result;
xmlNodeSetPtr nodeset;
char *search;
int nodes;

void xpath_search(int n) {
  context = xmlXPathNewContext(doc);
  if (context == NULL) {
    printf("Error in xmlXPathNewContext\n");
    return;
  }
  result = xmlXPathEvalExpression((unsigned char *)xpath, context);

  xmlXPathFreeContext(context);
  if (result == NULL) {
    printf("Error in xmlXPathEvalExpression\n");
    return;
  }
  if (xmlXPathNodeSetIsEmpty(result->nodesetval)) {
    xmlXPathFreeObject(result);
    debug_printf("No result for %s\n", xpath);
    return;
  }

  if (result) {
    nodeset = result->nodesetval;
    //		if (nodeset->nodeNr > 1)
    //			printf("multiple value %s %s\n",
    //					xmlNodeListGetString(doc,
    // nodeset->nodeTab[0]->xmlChildrenNode,
    // 1),
    //					xmlNodeListGetString(doc,
    // nodeset->nodeTab[1]->xmlChildrenNode,
    // 1)
    //					);
    search = (char *)xmlNodeListGetString(
        doc, nodeset->nodeTab[n]->xmlChildrenNode, 1);
    nodes = nodeset->nodeNr;
    // printf("searching %s res %s nodes \n",xpath,search,nodes);
    xmlXPathFreeObject(result);
    // return (char*)keyword;
  }
}

void xml_init() { doc = xmlParseMemory(xml_request, strlen(xml_request)); }
void xml_clean() {

  xmlFreeDoc(doc);
  xmlCleanupParser();
}

#define LIBRASHPA_XML_SEARCH(n, str)                                           \
  strcpy(xpath, str);                                                          \
  xpath_search(n);                                                             \
  if (search == NULL)                                                          \
    return RASHPA_XML_ERROR;

int parse_common(char *rashpa_manager_request, rashpa_ctx_t *link) {
  // roi number
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_roi_id);
  link->roi_number = nodes;

  // rashpa_bunch
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_rashpa_bunch);
  link->rashpa_bunch = atoi(search);

  // bdepth
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_backend_bdepth);
  link->bdepth = atoi(search);

  // link_type
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_type);
  int i = 0;
  while (RASHPA_LINK_NAME[i] != NULL &&
         strcmp(search, RASHPA_LINK_NAME[i]) != 0)
    i++;
  if (RASHPA_LINK_NAME[i] == NULL) {
    printf("RASHPA_XML_BAD_LINK\n");
    return RASHPA_XML_BAD_LINK;
  }
  link->link_type = i;
  debug_printf("XML_LINK_NAME=%s id %d\n", RASHPA_LINK_NAME[i], i);

  // w & h
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_roi_id);
  for (int roi_id = 0; roi_id < nodes; roi_id++) {
    LIBRASHPA_XML_SEARCH(roi_id, xp_rashpa_roi_w);
    link->roi_w[roi_id] = atoi(search);

    LIBRASHPA_XML_SEARCH(roi_id, xp_rashpa_roi_h);
    link->roi_h[roi_id] = atoi(search);
  }

  // nImages
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_backend_nImages);
  link->nImages = atoi(search);

  // gpu w, h
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_backend_gpu_w);
  link->gpu_w = atoi(search);
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_backend_gpu_h);
  link->gpu_h = atoi(search);

  // hostname
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_hostname);
  strcpy(link->hostname, search);

  return RASHPA_XML_OK;
}

int process_rashpa_telegram_for_romulu(char *rashpa_manager_request,
                                       rashpa_ctx_t *link, roce_ctx_t *roce) {
  xml_request = rashpa_manager_request;
  xml_init();
  char *endptr;

  if (parse_common(rashpa_manager_request, link) != RASHPA_XML_OK) {
    printf("error parse_common\n");
    return RASHPA_XML_ERROR;
  }

  // batch
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_backend_batch);
  link->batch = atoi(search);
  // printf("batch %d\n",link->batch);

  // pinnedbufSize
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_backend_savePinnedBuf);
  link->savePinnedBuf = atoi(search);

  // nIter
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_backend_nIter);
  link->nIter = atoi(search);

  // gpucode/gpustream
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_backend_gpucode);
  link->gpucode = atoi(search);
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_backend_gpustream);
  link->gpustream = atoi(search);

  // receiver_type
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_backend_receiver_type);
  int i = 0;
  while (ROMULU_RECEIVER[i] != NULL && strcmp(search, ROMULU_RECEIVER[i]) != 0)
    i++;
  if (ROMULU_RECEIVER[i] == NULL) {
    printf("RASHPA_XML_BAD_RECEIVER\n");
    return RASHPA_XML_BAD_RECEIVER;
  }
  link->receiver_type = i;

  if (link->receiver_type == RASHPA_RECEIVER_NVIDIA_CUDA_GPU ||
      link->receiver_type == RASHPA_RECEIVER_NVIDIA_CUDAMEM_GPU) {
    // dest_addr
    LIBRASHPA_XML_SEARCH(0, xp_rashpa_backend_gpuId);
    link->gpuId = strtod(search, &endptr);
  }

  if (link->link_type == RASHPA_LINK_ROCEV2_UC) {
    // nic_wire
    LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_nic);
    strcpy(link->nic, search);
    // RoCE service type
    LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_service_type);
    roce->service_type = atoi(search);
  }
  // gain_file
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_roi_gain_file);
  strcpy(link->gain_file, search);

  if (link->link_type == RASHPA_LINK_RSOCKET ||
      link->link_type == RASHPA_LINK_UDP ||
      link->link_type == RASHPA_LINK_UDP_JF) {
    // port
    LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_port);
    link->port = atoi(search);
  }

  LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_is_ib);
  roce->is_ib = atoi(search);

  LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_dlid);
  roce->dlid = atoi(search);

  // RoCE gid_index
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_gid_index);
  roce->gid_index = atoi(search);
  // roi : dst_x & dst_y
  for (int roi_id = 0; roi_id < link->roi_number; roi_id++) {
    LIBRASHPA_XML_SEARCH(roi_id, xp_rashpa_roi_dst_x);
    link->roi_dst_x[roi_id] = atoi(search);
    LIBRASHPA_XML_SEARCH(roi_id, xp_rashpa_roi_dst_y);
    link->roi_dst_y[roi_id] = atoi(search);
  }
  // //veto_h veto_l
  // LIBRASHPA_XML_SEARCH(0,xp_rashpa_veto_h);
  // link->veto_h = atoi(search);
  // LIBRASHPA_XML_SEARCH(0,xp_rashpa_veto_l);
  // link->veto_l = atoi(search);

  xml_clean();
  return RASHPA_XML_OK;
}

int process_rashpa_telegram_for_remu(char *rashpa_manager_request,
                                     rashpa_ctx_t *link,
                                     roce_ctx_t *roce) // gpu_ctxt_t*gpu
{
  char *endptr;
  xml_request = rashpa_manager_request;
  xml_init();

  parse_common(rashpa_manager_request, link);

  // nanodelay
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_nanodelay);
  link->nanodelay = strtoll(search, &endptr, 10);
  // printf("parsing nanodelay %ld",link->nanodelay);

  // data_source
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_data_source);
  link->data_source = atoi(search);

  if (link->link_type == RASHPA_LINK_ROCEV2_UC) {
    // remote_qpn & qkey
    for (int roi_id = 0; roi_id < link->roi_number; roi_id++) {
      LIBRASHPA_XML_SEARCH(roi_id, xp_rashpa_link_qpn);
      roce->remote_qpn[roi_id] = atoi(search);
      // qkey
      LIBRASHPA_XML_SEARCH(roi_id, xp_rashpa_link_rkey);
      roce->rkey[roi_id] = strtol(search, &endptr, 16);

      LIBRASHPA_XML_SEARCH(roi_id, xp_rashpa_link_daddr);
      roce->daddr[roi_id] = strtoll(search, &endptr, 16);
      printf("roce->daddr[roi_id] %p\n", roce->daddr[roi_id]);
    }
    // RoCE service type
    LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_service_type);
    roce->service_type = atoi(search);

    // RoCE gid_index
    LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_gid_index);
    roce->gid_index = atoi(search);

    // IB dlid
    LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_dlid);
    roce->dlid = atoi(search);

    LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_is_ib);
    roce->is_ib = atoi(search);

    // qkey
    LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_qkey);
    roce->qkey = strtol(search, &endptr, 16);

    // nic_wire
    LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_nic);
    strcpy(link->nic, search);
  }

  if (link->link_type == RASHPA_LINK_RSOCKET ||
      link->link_type == RASHPA_LINK_UDP ||
      link->link_type == RASHPA_LINK_UDP_JF) {
    // port
    LIBRASHPA_XML_SEARCH(0, xp_rashpa_link_port);
    link->port = atoi(search);
  }

  // roi : src_x & src_y & stride
  for (int roi_id = 0; roi_id < link->roi_number; roi_id++) {
    LIBRASHPA_XML_SEARCH(roi_id, xp_rashpa_roi_src_x);
    link->roi_src_x[roi_id] = atoi(search);
    LIBRASHPA_XML_SEARCH(roi_id, xp_rashpa_roi_src_y);
    link->roi_src_y[roi_id] = atoi(search);
  }

  LIBRASHPA_XML_SEARCH(0, xp_rashpa_roi_stride)
  link->roi_stride = atoi(search);
  LIBRASHPA_XML_SEARCH(0, xp_rashpa_gpu_stride)
  link->gpu_stride = atoi(search);

  xml_clean();
  return RASHPA_XML_OK;
}
