#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE

#include <arpa/inet.h>
#include <fcntl.h>
#include <malloc.h>
#include <netdb.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "librashpa.h"
#include "rashpa_init.h"
#include "rashpa_roce_ud.h"
#include <infiniband/verbs.h>

// 1) post nWr/2 requests
// if (ibv_post_recv(roce->qp[0], wr, &bad_wr))
// {
// 	printf("error ibv_post_recv 1\n");
// 	return -1;
// }

// //2) poll 1
// do
// {
// 	n = ibv_poll_cq(roce->cq[0], 1, wc);
// } while ( (n<1) && ((wc[0].wc_flags & IBV_WC_WITH_IMM) != IBV_WC_WITH_IMM));

// psn = ntohl(wc[0].imm_data) % h[0];
// printf("psn %d img num %d h[0] %d\n",psn,ntohl(wc[0].imm_data)/h[0],h[0]);
// if (psn)
// {
// 	//3) post nWr/2-psn
// 	if (ibv_post_recv(roce->qp[0], &wr[psn], &bad_wr))
// 	{
// 		printf("error ibv_post_recv 21\n");
// 		return -1;
// 	}
// 	//4)post nWr/2
// 	if (ibv_post_recv(roce->qp[0], wr, &bad_wr))
// 	{
// 		printf("error ibv_post_recv 22\n");
// 		return -1;
// 	}

// 	if (psn>h[0]/2) //CHECKME issue in 512x1024 ???
// 	{
// 		if (ibv_post_recv(roce->qp[0], wr, &bad_wr))
// 		{
// 			printf("error ibv_post_recv 23\n");
// 			return -1;
// 		}
// 		p=0;
// 		do
// 		{
// 			n = ibv_poll_cq(roce->cq[0], nWr/2 - p, wc);
// 			p += n;
// 		} while (p < (nWr/2));
// 	}
// 	//5)poll nWr - psn
// 	p=0;
// 	do
// 	{
// 		n = ibv_poll_cq(roce->cq[0], nWr-psn - p, wc);
// 		p += n;
// 	} while (p < (nWr-psn));

// 	if(psn>nWr/2)
// 	{
// 		p=0;
// 		do
// 		{
// 			n = ibv_poll_cq(roce->cq[0], nWr/2 - p, wc);
// 			p += n;
// 		} while (p < (nWr/2));
// 	}
// }

// FIXME somewhere else
extern void allocate_gpu_device_memory(size_t, uint64_t, int);
extern void select_cpu(int);

volatile int gpustate;

roce_ctx_t g_roce;
rashpa_ctx_t g_rashpa;

void create_ib_context() {
  roce_ctx_t *roce = &g_roce;
  rashpa_ctx_t *link = &g_rashpa;
  int ib_port = 1;

  int num_devices;

  // get infiniband device
  // printf("ibv_get_device_list, searching %s\n",link->nic);
  roce->dev_list = ibv_get_device_list(&num_devices);
  if (!roce->dev_list) {
    perror("Failed to get IB devices list");
    return;
  }

  roce->ib_dev = NULL;
  for (int i = 0; roce->dev_list[i]; i++)
    if (strcmp(ibv_get_device_name(roce->dev_list[i]), link->nic) == 0) {
      roce->ib_dev = roce->dev_list[i];
      break;
    }
  if (!roce->ib_dev) {
    fprintf(stderr, "No IB devices found on wire %s\n", link->nic);
    release_ib_context(RELEASE_IBV_FREE_DEVICE_LIST, 0);
    return;
  }

  // create context
  // printf("ibv_open_device\n");
  roce->context = ibv_open_device(roce->ib_dev);
  if (!roce->context) {
    fprintf(stderr, "Couldn't get context for %s\n",
            ibv_get_device_name(roce->ib_dev));
    release_ib_context(RELEASE_IBV_CLOSE_DEVICE, 0);
    return;
  }

  struct ibv_port_attr port_info = {};
  if (ibv_query_port(roce->context, ib_port, &port_info)) {
    fprintf(stderr, "Unable to query port info for port %d\n", ib_port);
    release_ib_context(RELEASE_IBV_CLOSE_DEVICE, 0);
    return;
  }
  int mtu = 1 << (port_info.active_mtu + 7);

  // Protection Domain
  // debug_printf("ibv_alloc_pd\n");
  roce->pd = ibv_alloc_pd(roce->context);
  if (!roce->pd) {
    fprintf(stderr, "Couldn't allocate PD\n");
    release_ib_context(RELEASE_IBV_DESTROY_COMP_CHANNEL, 0);
    return;
  }

  // we do not need channel (for events mode)
  roce->channel = NULL;

  debug_printf("created ib_context for device %s (MTU %d)\n",
               ibv_get_device_name(roce->ib_dev), mtu);
}

void release_ib_context(int action, int i) {
  roce_ctx_t *roce = &g_roce;
  int num_comp;
  int j = 0;
  struct ibv_wc wc;
  struct ibv_qp_attr attr;

  sleep(1); // wait for completion outsandirng requests

  switch (action) {

  case RELEASE_IBV_POLL:
    debug_printf("clear ibv_poll_cq %d\n", i);
    do {
      j++;
      num_comp = ibv_poll_cq(roce->cq[i], 1, &wc);
    } while (num_comp > 0 || j < 60000); // > wq max 326536
  case RELEASE_RESET:
    debug_printf("ibv_modify_qp to RESET\n");
    attr.qp_state = IBV_QPS_RESET;
    if (ibv_modify_qp(roce->qp[i], &attr, IBV_QP_STATE)) {
      debug_printf("Failed to modify QP to RESET\n");
    }
  case RELEASE_IBV_DESTROY_QP:
    debug_printf("ibv_destroy_qp %d\n", i);
    if (ibv_destroy_qp(roce->qp[i])) {
      debug_printf("Error, ibv_destroy_qp() failed\n");
      return;
    }

  case RELEASE_IBV_DESTROY_CQ:
    if (i == 0) {
      debug_printf("ibv_destroy_cq %d\n", i);
      if (ibv_destroy_cq(roce->cq[i])) {
        debug_printf("Error, ibv_destroy_cq() failed\n");
      }
    }
    debug_printf("ibv_destroy_cq DONE\n");

  case RELEASE_IBV_DEREG_MR:
    if (i == 0) {
      debug_printf("ibv_dereg_mr_gth no implemented\n");
      // if () ibv_dereg_mr(roce->mr_gth);
      debug_printf("ibv_dereg_mr\n");

      // if (roce->mr)
      ibv_dereg_mr(roce->mr);
    }

  case RELEASE_IBV_DEALLOC_PD:
    if (i == 0) {
      debug_printf("ibv_dealloc_pd\n");
      ibv_dealloc_pd(roce->pd);
    }

  case RELEASE_IBV_DESTROY_COMP_CHANNEL:
    if (i == 0) {
      debug_printf("ibv_destroy_comp_channel\n");
      if (roce->channel)
        ibv_destroy_comp_channel(roce->channel);
    }

  case RELEASE_IBV_CLOSE_DEVICE:
    if (i == 0) {
      debug_printf("ibv_close_device\n");
      ibv_close_device(roce->context);
    }

  case RELEASE_IBV_FREE_DEVICE_LIST:
    if (i == 0) {
      debug_printf("ibv_free_device_list\n");
      ibv_free_device_list(roce->dev_list);
    }

  case RELEASE_GTH_MEM: // BackEnd only
    if (i == 0) {
      // debug_printf("free gth memory done somewhere else at
      // %p\n",roce->buf_gth);
      // if (roce->buf_gth) free(roce->buf_gth);
    }
  }
  // debug_printf("end release_ib_context %d\n",i);
}

int create_ib_cq_qprtr(int direction, int roi_id) {
  roce_ctx_t *roce = &g_roce;
  rashpa_ctx_t *link = &g_rashpa;
  int ib_port = 1;
  int h = link->roi_h[roi_id];
  int rashpa_bunch = rashpa_bunch ? link->rashpa_bunch : 2;

  debug_printf("ibv_create_cq of %d wr roi_id %d max_recv_wr %d %d %d %d\n", h,
               roi_id, 2 * rashpa_bunch * h, link->roi_h[0],
               link->roi_h[roi_id], rashpa_bunch);
  // FIXME choose relevant cqe size
  if (direction == RASHPA_BACKEND)
    roce->cq[roi_id] =
        ibv_create_cq(roce->context, 2 * rashpa_bunch * h + h, NULL, NULL, 0);
  if (direction == RASHPA_DETECTOR)
    roce->cq[roi_id] = ibv_create_cq(roce->context, 2 * h, NULL, NULL, 0);

  if (!roce->cq[roi_id]) {
    debug_printf("Couldn't create CQ (rashpa_bunch x h to high?) %d\n", h);
    release_ib_context(RELEASE_IBV_DEREG_MR, roi_id);
    return RASHPA_LINK_ERROR;
  }

  // Queue Pair
  struct ibv_qp_init_attr qp_init_attr;
  memset(&qp_init_attr, 0, sizeof(struct ibv_qp_init_attr));
  qp_init_attr.send_cq = roce->cq[roi_id];
  qp_init_attr.recv_cq = roce->cq[roi_id];
  qp_init_attr.cap.max_send_wr = (direction == RASHPA_DETECTOR ? 2 * h : 2);
  qp_init_attr.cap.max_send_sge = 2;
  // FIXME +h lines ?//FIXME choose relevant cqe size
  qp_init_attr.cap.max_recv_wr =
      (direction == RASHPA_BACKEND ? 2 * rashpa_bunch * h + h : 2);
  qp_init_attr.cap.max_recv_sge = 2;
  qp_init_attr.qp_type = IBV_QPT_UD;

  debug_printf("ibv_create_qp %s for roi_id %d\n",
               direction == RASHPA_DETECTOR ? "send" : "recv", roi_id);
  roce->qp[roi_id] = ibv_create_qp(roce->pd, &qp_init_attr);
  if (!roce->qp[roi_id]) {
    debug_printf("Error, ibv_create_qp() failed\n");
    release_ib_context(RELEASE_IBV_DESTROY_CQ, roi_id);
    return RASHPA_LINK_ERROR;
  }

  debug_printf("ibv_modify_qp to INIT\n");
  struct ibv_qp_attr attr;
  memset(&attr, 0, sizeof(struct ibv_qp_attr));
  attr.qp_state = IBV_QPS_INIT;
  attr.pkey_index = 0;
  attr.port_num = ib_port;
  attr.qkey = 0xCAFECAFE;
  if (ibv_modify_qp(roce->qp[roi_id], &attr,
                    IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT |
                        IBV_QP_QKEY)) {
    debug_printf("Failed to modify QP to INIT\n");
    release_ib_context(RELEASE_IBV_DESTROY_QP, roi_id);
    return RASHPA_LINK_ERROR;
  }

  debug_printf("ibv_modify_qp to RTR\n");
  memset(&attr, 0, sizeof(struct ibv_qp_attr));
  attr.qp_state = IBV_QPS_RTR;
  if (ibv_modify_qp(roce->qp[roi_id], &attr, IBV_QP_STATE)) {
    debug_printf("Failed to modify QP to RTR\n");
    release_ib_context(RELEASE_IBV_DESTROY_QP, roi_id);
    return RASHPA_LINK_ERROR;
  }

  debug_printf("ibv_query_gid\n");
  union ibv_gid gid;
  if (ibv_query_gid(roce->context, ib_port, 1, &gid)) {
    debug_printf("ibv_query_gid\n");
    release_ib_context(RELEASE_IBV_DESTROY_QP, roi_id);
    return RASHPA_LINK_ERROR;
  }
  if (direction == RASHPA_DETECTOR) {
    gid.raw[10] = 255;
    gid.raw[11] = 255;

    struct hostent *rhost = gethostbyname(link->hostname);
    if (rhost == NULL) {
      in_addr_t addr = inet_addr(link->hostname);
      if (addr != INADDR_NONE) {
        memcpy(&gid.raw[12], &addr, 4);
      } else {
        fprintf(stderr, "Error, gethostbyname()/inet_addr() failed\n");
        release_ib_context(RELEASE_IBV_DESTROY_CQ, roi_id);
        return RASHPA_LINK_ERROR;
      }
    } else {
      gid.raw[12] = rhost->h_addr_list[0][0];
      gid.raw[13] = rhost->h_addr_list[0][1];
      gid.raw[14] = rhost->h_addr_list[0][2];
      gid.raw[15] = rhost->h_addr_list[0][3];
    }

    debug_printf("the remote ip addr is %d.%d.%d.%d\n", gid.raw[12],
                 gid.raw[13], gid.raw[14], gid.raw[15]);
  }

  debug_printf("ibv_create_ah\n");
  struct ibv_ah_attr ah_attr;
  memset(&ah_attr, 0, sizeof(ah_attr));
  ah_attr.is_global = 1;
  ah_attr.grh.dgid = gid;
  ah_attr.port_num = ib_port;
  ah_attr.grh.sgid_index = 0; // <===== TO DO choose the right index
  ah_attr.grh.hop_limit = 0xFF;
  ah_attr.grh.traffic_class = 1;
  ah_attr.dlid = 0;
  ah_attr.sl = 0;
  ah_attr.src_path_bits = 0;
  roce->ah = ibv_create_ah(roce->pd, &ah_attr);
  if (!roce->ah) {
    debug_printf("Error, ibv_create_ah() failed\n");
    release_ib_context(RELEASE_IBV_DESTROY_QP, roi_id);
    return RASHPA_LINK_ERROR;
  }
  debug_printf("ibvqprtr done for roi_id %d\n", roi_id);
  return RASHPA_LINK_OK;
}

/**
 * init with parameters from RASPA telegram
 */

int rashpa_rocev2_ud_backend_receiver_init(int roi_id) {
  roce_ctx_t *roce = &g_roce;
  rashpa_ctx_t *link = &g_rashpa;
  int rashpa_bunch = link->rashpa_bunch;
  // FIXME sizeof(float)
  int bdepth = link->bdepth;
  int gpu_w = link->gpu_w * bdepth;
  int gpu_h = link->gpu_h;
  int page_size = sysconf(_SC_PAGESIZE);

  if (roi_id == 0) {
    create_ib_context();

    // 40 RoCEv2 UD extra bytes
    roce->buf_gth = memalign(page_size, 40);
    if (!link->dest_addr || !roce->buf_gth) {
      debug_printf("Couldn't allocate memory work buf. %p %p\n",
                   link->dest_addr, roce->buf_gth);
      release_ib_context(RELEASE_IBV_FREE_DEVICE_LIST, 0);
      return RASHPA_LINK_ERROR;
    }

    // Memory Region
    debug_printf("ibv_reg_mr link->dest_addr %p size %d\n", link->dest_addr,
                 gpu_w * gpu_h * 2 * rashpa_bunch);
    // link->dest_addr = memalign(page_size, gpu_w * gpu_h * 2 * rashpa_bunch);

    // char devname[128]="/dev/hugepages/rashpadata";

    // int fd = open(devname, O_RDWR|O_CREAT, 0755);
    // if (fd < 0) {
    // 	perror("/dev/hugepages/rashpadata Open failed");
    // 	exit(1);
    // }
    // void* addr = mmap(	NULL,
    // 					2 * rashpa_bunch * gpu_w * gpu_h ,
    // 					PROT_READ|PROT_WRITE,
    // 					MAP_SHARED|MAP_HUGETLB,
    // 					fd,
    // 					0);
    // if (addr == MAP_FAILED)
    // {
    // 	perror("mmap ");
    // 	printf("of %d error\n",2 * rashpa_bunch * gpu_w * gpu_h );
    // 	unlink("/dev/hugepages/rashpadata");
    // 	exit(1);
    // }
    // link->dest_addr =  addr;

    // CHECKME TEST ONLY

    roce->mr =
        ibv_reg_mr(roce->pd, link->dest_addr, gpu_w * gpu_h * 2 * rashpa_bunch,
                   IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
    roce->mr_gth = ibv_reg_mr(roce->pd, roce->buf_gth, 40,
                              IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE);
    if (!roce->mr) {
      debug_printf(
          "Couldn't register MR (nv_peer_mem missing, registred size ? ?)\n");
      release_ib_context(RELEASE_IBV_DEALLOC_PD, 0);
      return RASHPA_LINK_ERROR;
    }
    if (!roce->mr_gth) {
      debug_printf("Couldn't register MR GTH\n");
      release_ib_context(RELEASE_IBV_DEALLOC_PD, 0);
      return RASHPA_LINK_ERROR;
    }
  }
  // debug_printf("create_ib_cq_qprtr for roi_id %d\n",roi_id);
  create_ib_cq_qprtr(RASHPA_BACKEND, roi_id);

  // debug_printf("rashpa_roce_ud_backend_receiver_init done  device %s\n",
  // ibv_get_device_name(roce->ib_dev));

  return RASHPA_LINK_OK;
}

extern volatile int waitFlag;
volatile int *pwaitFlag;

void *rashpa_rocev2_ud_backend_receiver_mainloop_single_thread(void *arg) {
  pwaitFlag = &waitFlag;

  roce_ctx_t *roce = &g_roce;
  rashpa_ctx_t *link = &g_rashpa;
  int rashpa_bunch = link->rashpa_bunch;
  int roi_number = link->roi_number;
  int nImages = link->nImages;
  int nIter = link->nIter;
  int bdepth = link->bdepth;
  int gpu_w = link->gpu_w * bdepth;
  int gpu_h = link->gpu_h;
  int *w = link->roi_w;
  int *h = link->roi_h;
  int *off_x = link->roi_dst_x;
  int *off_y = link->roi_dst_y;
  int page_size = sysconf(_SC_PAGESIZE);

  struct ibv_sge **sg = (struct ibv_sge **)memalign(
      page_size, roi_number * sizeof(struct ibv_sge *));
  struct ibv_recv_wr **wr = (struct ibv_recv_wr **)memalign(
                         page_size, roi_number * sizeof(struct ibv_recv_wr *)),
                     *bad_wr;

  int rashpa_wr = 0;
  for (int roi_id = 0; roi_id < roi_number; roi_id++) {
    rashpa_wr += 2 * rashpa_bunch * h[roi_id];

    sg[roi_id] = (struct ibv_sge *)memalign(
        page_size, 4 * rashpa_bunch * h[roi_id] * sizeof(struct ibv_sge));
    wr[roi_id] = (struct ibv_recv_wr *)memalign(
        page_size, 2 * rashpa_bunch * h[roi_id] * sizeof(struct ibv_recv_wr));

    memset(sg[roi_id], 0, 4 * h[roi_id] * sizeof(struct ibv_sge));
    memset(wr[roi_id], 0,
           2 * rashpa_bunch * h[roi_id] * sizeof(struct ibv_recv_wr));

    debug_printf(
        "receiver buffer %p roi_id %d roce->remote_qpn[roi_id] %d gpu_w %d "
        "gpu_h %d _w %d h %d offset_x %d offset_y %d rashpa_wr %d\n",
        link->dest_addr, roi_id, roce->remote_qpn[roi_id], gpu_w, gpu_h,
        w[roi_id], h[roi_id], off_x[roi_id], off_y[roi_id], rashpa_wr);
  }
  struct ibv_wc *wc = (struct ibv_wc *)memalign(
      page_size, 2 * rashpa_bunch * h[0] * sizeof(struct ibv_wc));

  /**
   *
   * cpu affinity for efficient nic/gpu communication
   *
   */
  select_cpu(RASHPA_BACKEND);

  int nRecvImg = 0;
  int bank = 1;
  int toofast = 0;

  for (int roi_id = 0; roi_id < roi_number; roi_id++) {
    for (int i = 0; i < 2 * rashpa_bunch * h[roi_id]; i++) {
      // debug_printf("ROMULU main loop wr_id%d\n",i);

      wr[roi_id][i].wr_id = i;
      wr[roi_id][i].next = ((i == (rashpa_bunch * h[roi_id] - 1)) ||
                            (i == (2 * rashpa_bunch * h[roi_id] - 1)))
                               ? NULL
                               : &wr[roi_id][i + 1];
      wr[roi_id][i].sg_list = &(sg[roi_id][2 * i]);
      wr[roi_id][i].num_sge = 2;
      sg[roi_id][2 * i].length = 40;
      sg[roi_id][2 * i].lkey = roce->mr_gth->lkey;
      sg[roi_id][2 * i].addr = (uint64_t)roce->buf_gth;
      sg[roi_id][2 * i + 1].length = w[roi_id] * bdepth;
      sg[roi_id][2 * i + 1].lkey = roce->mr->lkey;
      sg[roi_id][2 * i + 1].addr =
          (uint64_t)link->dest_addr +
          +(i / h[roi_id]) * gpu_w * (gpu_h - h[roi_id]) +
          off_y[roi_id] * gpu_w + off_x[roi_id] * bdepth + i * gpu_w;
    }
  }
  // debug_printf("ROMULU main loop ready for %d Iter  of %d images.
  // rashpa_bunch %d\n",nIter,nImages, link->rashpa_bunch);

  struct timespec startTime, endTime;
  unsigned int psn, missed;
  int start, nCompletion, num_comp, diff, nbWr, l, counter, wrn;

  bank = 1;
  nRecvImg = -1;
  psn = 0;         // packet sequence number, read from net
  missed = 0;      // total missed packet
  nCompletion = 0; // number of lines received
  start = 1;
  diff = 0; // psn - nCompletion, should always be 0
  num_comp = 0;
  l = 0;

  char msg[1024];
  strcpy(msg, RASHPA_LINK_NAME[link->link_type]);
  int status;
  *pwaitFlag = GPUIDLE;
  int wrIn = 0, p;
  int onum;

  /////////////////////////////////
  // From now on, entering datapath
  //    speed matters !!!
  //

  for (int roi_id = 0; roi_id < roi_number; roi_id++) {

    // 1) post nWr/2 requests
    if (ibv_post_recv(roce->qp[roi_id], &wr[roi_id][0], &bad_wr)) {
      printf("error ibv_post_recv 1\n");
      return -1;
    }
    wrIn += rashpa_bunch * h[roi_id];
  }

  // 2) poll 1 and extract first psn MUST POLL ALL CQ !!!!
  // debug_printf("waiting SoF\n");
  while (link->romulu_state == ROMULU_RUNNING &&
         !ibv_poll_cq(roce->cq[roi_number - 1], 1, wc))
    ;
  wrIn -= 1;
  psn = ntohl(wc[0].imm_data) % h[0];

  if (link->romulu_state != ROMULU_RUNNING) {
    debug_printf("terminating.....\n");
    goto _end_;
  }

  // if not 0 post h - offset extra wr to be aligned, i.e. &wr[offset]
  if (psn) {
    printf("================psn %d\n", psn);
    int roi_id = 0;
    // 3) post nWr/2-psn
    // post extra recv wr to stay aligned              -1
    if (ibv_post_recv(roce->qp[0], &wr[0][(rashpa_bunch - 1) * h[0] + psn],
                      &bad_wr)) {
      debug_printf("error ibv_post_recv 3\n");
      release_ib_context(RELEASE_IBV_DESTROY_QP, 0);
      return NULL;
    }
    wrIn += h[0] - psn;
    // 4)post nWr/2
    if (ibv_post_recv(roce->qp[0], &wr[0][(rashpa_bunch - 1) * h[0]],
                      &bad_wr)) {
      debug_printf("error ibv_post_recv 4\n");
      release_ib_context(RELEASE_IBV_DESTROY_QP, 0);
      return NULL;
    }
    wrIn += h[0];

    // CHECK THIS
    if (psn > h[0] / 2) {
      if (ibv_post_recv(roce->qp[0], &wr[0][(rashpa_bunch - 1) * h[0]],
                        &bad_wr)) {
        printf("error ibv_post_recv 4bis\n");
        return -1;
      }
      p = 0;
      do {
        l = ibv_poll_cq(roce->cq[0], h[0] / 2 - p, wc);
        p += l;
      } while (p < (h[0] / 2));
    }

    // 5)poll nWr - psn
    p = 0;
    do {
      l = ibv_poll_cq(roce->cq[0], h[0] - psn - p, wc);
      p += l;
      wrIn -= l;
    } while (p < (h[0] - psn));

    if (psn > h[0] / 2) {
      p = 0;
      do {
        l = ibv_poll_cq(roce->cq[0], h[0] / 2 - p, wc);
        p += l;
        wrIn -= l;
      } while (p < (h[0] / 2));
    }
    psn = ntohl(wc[l - 1].imm_data) % h[0];
  }

  debug_printf("\n\n----->>> psn %d wrIn %d numcomp %d<<<<--------------\n\n",
               psn, wrIn, num_comp);

  /**
   *
   * Main receiver loop
   *
   */
  clock_gettime(CLOCK_MONOTONIC, &startTime);

  while (nRecvImg * rashpa_bunch < nIter * nImages &&
         link->romulu_state == ROMULU_RUNNING) {
    // debug_printf("loop nRecvImg %d nImgOnGpu %d counter %d wrn %d\n",
    // 				nRecvImg, link->nImgOnGpu,counter ,wrn);
    // debug_printf("loop %d\n",wrIn);
    for (int roi_id = 0; roi_id < roi_number; roi_id++) {
      status = ibv_post_recv(
          roce->qp[roi_id],
          &(wr[roi_id][(bank ? 0 : rashpa_bunch * h[roi_id])]), &bad_wr);
      if (status) {
        debug_printf("ibv_post_recv main loop failure status %d (try larger "
                     "wc) roi_id %d failed in wr %ld at image %d\n",
                     status, roi_id, (*bad_wr).wr_id, nRecvImg);
        release_ib_context(RELEASE_IBV_DESTROY_QP, roi_id);
        return NULL;
      }
      wrIn += rashpa_bunch * h[roi_id];
    }

    for (int roi_id = 0; roi_id < roi_number; roi_id++) {
      nbWr = rashpa_bunch * h[roi_id];
      num_comp = 0;
      do {
        // debug_printf("ibv_poll_cq num_comp %d roi_id %d qp_num %d
        // \n",num_comp, roi_id,roce->qp[roi_id]->qp_num);
        l = ibv_poll_cq(roce->cq[roi_id], nbWr - num_comp, wc);
        wrIn -= l;
        num_comp += l;
      } while ((num_comp < nbWr) && (link->romulu_state == ROMULU_RUNNING));
      nCompletion += num_comp;
      /* verify the completion status just to be sure */
      // int status = IBV_WC_SUCCESS;
      // for (int i=0; i<l;i++)
      // {
      // 	status = status && (wc[i].status == IBV_WC_SUCCESS);

      // }
      // if (status != IBV_WC_SUCCESS)
      // {
      // 	debug_printf("Failed status near %s (%d) for wr_id %d
      // nCompletion %d\n",
      // 		ibv_wc_status_str(wc[0].status),wc[0].status,
      // (int)wc[0].wr_id,nCompletion);
      // 	exit(0);
      // }

      /**
       * take care of missing paket if any ...
       *
       *
       */
      // psn = ntohl(wc[l-1].imm_data) % h[0];
      // debug_printf("psn %d\n",psn%h[0]);

      onum = psn;
      psn = ntohl(wc[l - 1].imm_data);
      if ((psn - onum) > (rashpa_bunch * h[0]) || ((psn % (h[0])) != 0)) {
        // if (missed==0)
        printf(A_C_RED "#### PACKET DROP psn %d at image %d ####\n" A_C_RESET,
               psn % h[0], nRecvImg);
        missed++;
      }
    }

    bank = !bank; // nRecvImg%2 instead ?

    // signal cuda kernel each bunch images
    if (*pwaitFlag != GPUIDLE) {
      // printf(A_C_RED "#### SLOW GPU code %d img %d ####\n"
      // A_C_RESET,*pwaitFlag,nRecvImg);
      toofast += 1;
      // exit(0);
    }
    // discard first bunch (synchronization)
    if (nRecvImg > -1) {
      // pthread_cond_signal(&link->condition);
      *pwaitFlag = 2222;
    }
    nRecvImg++;
    // if (nRecvImg%nImages==0)
    // 	printf("nIter=%d received %d pre-processed %d\t
    // %d\n",nRecvImg/nImages,nRecvImg,link->nImgOnGpu,
    // nRecvImg-link->nImgOnGpu);
  } // end main loop

  clock_gettime(CLOCK_MONOTONIC, &endTime);
  // purge NIC

  for (int roi_id = 0; roi_id < roi_number; roi_id++) {
    int i = 0;
    do {
      i++;
      num_comp = ibv_poll_cq(roce->cq[roi_id], nbWr / 2, wc);
    } while (num_comp > 0 || i < 40000);
  }

  // give some time to finish process the kernel
  usleep(500000);

  if ((missed > 0) || (link->nImgOnGpu * rashpa_bunch < nIter * nImages) ||
      toofast) {
    strcat(msg, "_ISSUE ");
    if (missed > 1)
      strcat(msg, "cause='PACKET_MISS' ");
    if (toofast > 0)
      strcat(msg, "cause='GPU_SLOW' ");
    if (link->nImgOnGpu * rashpa_bunch < nIter * nImages)
      strcat(msg, "cause='IMGONGPU_LOW' ");

    debug_printf("RoCEv2 nCompletion %d psn %d" A_C_RED
                 " missed %d toofast %d\n" A_C_RESET,
                 nCompletion, psn, missed, toofast);
    debug_printf(A_C_RED "\n\n CAUTION : GPU compute too slow, missing data "
                         "(%d / %d)\n\n" A_C_RESET,
                 link->nImgOnGpu * rashpa_bunch, nRecvImg * rashpa_bunch);
  } else {
    strcat(msg, "_OK");
  }

_end_:
  // in case of gpu computation too slow
  for (int i = 0; i < missed; i++) {
    // pthread_cond_signal(&link->condition);
    // debug_printf( "extra signaling\n");
    *pwaitFlag = 2222;
    usleep(500);
  }
  *pwaitFlag = GPUIDLE;
  // be prepared for the next loop
  // nRecvImg = 0 ;
  usleep(500000);
  displayProfilingResult(msg, &endTime, &startTime);

  link->romulu_state = ROMULU_LINK_READY;

  // debug_printf("clear ibv_poll_cq\n");
  for (int roi_id = 0; roi_id < roi_number; roi_id++) {
    int j = 0;
    do {
      j++;
      num_comp = ibv_poll_cq(roce->cq[roi_id], nbWr, wc);
      usleep(500);

    } while (num_comp > 0 || j < 20000);
  }

  // debug_printf("free\n");
  for (int roi_id = 0; roi_id < roi_number; roi_id++) {
    free(sg[roi_id]);
    free(wr[roi_id]);
  }
  free(wc);

  return NULL;
}

int rashpa_rocev2_ud_detector_init(int roi_id) {
  rashpa_ctx_t *link = &g_rashpa;
  roce_ctx_t *roce = &g_roce;
  void *src_addr = link->source_addr;
  uint64_t totalSize = link->dsSize;

  if (roi_id == 0) {
    create_ib_context();

    // Memory Region
    // debug_printf("ibv_reg_mr %p dsSize %ld\n",src_addr,totalSize);
    if (!src_addr) {
      fprintf(stderr, "Couldn't allocate work buf.\n");
      release_ib_context(RELEASE_IBV_DESTROY_COMP_CHANNEL, roi_id);
      return RASHPA_LINK_ERROR;
    }
    roce->mr =
        ibv_reg_mr(roce->pd, src_addr, totalSize, IBV_ACCESS_LOCAL_WRITE);
    if (!roce->mr) {
      fprintf(stderr,
              "Couldn't register detector MR  errno %d addr %p size %ld\n",
              errno, src_addr, totalSize);
      release_ib_context(RELEASE_IBV_DEALLOC_PD, roi_id);
      return RASHPA_LINK_ERROR;
    }
  }
  // debug_printf("call create_ib_cq_qprtr roi_id %d\n",roi_id);
  if (create_ib_cq_qprtr(RASHPA_DETECTOR, roi_id) != RASHPA_LINK_OK)
    return RASHPA_LINK_ERROR;

  // Modify QP to RTS
  debug_printf("ibv_modify_qp to RTS\n");
  struct ibv_qp_attr attr;
  memset(&attr, 0, sizeof(attr));
  attr.qp_state = IBV_QPS_RTS;
  attr.sq_psn = 1234;
  if (ibv_modify_qp(roce->qp[roi_id], &attr, IBV_QP_STATE | IBV_QP_SQ_PSN)) {
    fprintf(stderr, "Failed to modify QP to RTS\n");
    release_ib_context(RELEASE_IBV_DESTROY_QP, roi_id);
    return RASHPA_LINK_ERROR;
  }

  /**
   * cpu affinity
   *
   */
  select_cpu(RASHPA_DETECTOR);

  return RASHPA_LINK_OK;
}

void *rashpa_rocev2_ud_detector_mainloop_single_thread(void *arg) {
  roce_ctx_t *roce = &g_roce;
  rashpa_ctx_t *link = &g_rashpa;

  int nImages = link->nImages;
  int bdepth = link->bdepth;
  int image_W = link->hsz * bdepth;
  int image_H = link->vsz;
  int *w = link->roi_w;
  int *h = link->roi_h;
  int *off_x = link->roi_src_x;
  int *off_y = link->roi_src_y;
  int page_size = sysconf(_SC_PAGESIZE);

  struct ibv_wc *wc =
      (struct ibv_wc *)malloc(8 * image_H * sizeof(struct ibv_wc));
  struct ibv_sge **sg =
      (struct ibv_sge **)malloc(link->roi_number * sizeof(struct ibv_sge *));
  struct ibv_send_wr **wr = (struct ibv_send_wr **)malloc(
                         link->roi_number * sizeof(struct ibv_send_wr *)),
                     *bad_wr;

  int rashpa_wr = 0;

  // debug_printf("CHECK THIS Img_W %d Img_H %d w %d h %d\n",image_W, image_H,
  // w[0], h[0]);

  for (int roi_id = 0; roi_id < link->roi_number; roi_id++) {
    rashpa_wr += h[roi_id];
    sg[roi_id] = (struct ibv_sge *)memalign(
        page_size, 2 * h[roi_id] * sizeof(struct ibv_sge));
    wr[roi_id] = (struct ibv_send_wr *)memalign(
        page_size, 2 * h[roi_id] * sizeof(struct ibv_send_wr));
    memset(sg[roi_id], 0, 2 * h[roi_id] * sizeof(struct ibv_sge));
    memset(wr[roi_id], 0, 2 * h[roi_id] * sizeof(struct ibv_send_wr));

    debug_printf("rashpa_rocev2_ud_detector_mainloop_single_thread roi_id %d "
                 "roce->remote_qpn[roi_id] %d Img_W %d Img_H %d w %d h %d "
                 "offset_x %d offset_y %d\n",
                 roi_id, roce->remote_qpn[roi_id], image_W, image_H, w[roi_id],
                 h[roi_id], off_x[roi_id], off_y[roi_id]);
  }

#define CREATE_WR_DETECTOR                                                     \
  sg[roi_id][i].addr =                                                         \
      (uint64_t)link->source_addr + counter * image_W * image_H +              \
      (i / h[roi_id]) * image_W * (image_H - h[roi_id]) +                      \
      off_y[roi_id] * image_W + i * image_W + off_x[roi_id] * bdepth;          \
                                                                               \
  wr[roi_id][i].imm_data = htonl(pktCounter++);

  // commonn to all requests
  for (int roi_id = 0; roi_id < link->roi_number; roi_id++)
    for (int i = 0; i < h[roi_id]; i++) {
      wr[roi_id][i].wr_id = i;
      wr[roi_id][i].opcode = IBV_WR_SEND_WITH_IMM;
      wr[roi_id][i].send_flags =
          IBV_SEND_SIGNALED; // try UNSIGNALED qp_init_attr.sq_sig_all=0
      wr[roi_id][i].wr.ud.remote_qpn = roce->remote_qpn[roi_id];
      wr[roi_id][i].wr.ud.ah = roce->ah;
      wr[roi_id][i].wr.ud.remote_qkey = roce->qkey;
      wr[roi_id][i].next = (i == (h[roi_id] - 1) || i == (h[roi_id] / 2 - 1))
                               ? NULL
                               : &wr[roi_id][i + 1];

      wr[roi_id][i].sg_list = &sg[roi_id][i];
      wr[roi_id][i].num_sge = 1;
      sg[roi_id][i].length = w[roi_id] * bdepth;
      sg[roi_id][i].lkey = roce->mr->lkey;
    }

  int num_comp, nCompletion, bank, extra, start, counter, wrn;
  unsigned int pktCounter;

  start = 1;
  bank = 0;
  nCompletion = 0;
  num_comp = 0;
  pktCounter = 0; // never reset to 0
  extra = 0;
  counter = 0; // image counter from 0 to nImages
  wrn = 0;

  struct timespec delay, rem;
  delay.tv_nsec = link->nanodelay;

  debug_printf("Nanodelay %d\n", delay.tv_nsec);

  // a first batch of WR to half fill HCA
  for (int roi_id = 0; roi_id < link->roi_number; roi_id++) {
    for (int i = 0; i < h[roi_id]; i++) {
      CREATE_WR_DETECTOR;
    }

    if (ibv_post_send(roce->qp[roi_id], &(wr[roi_id][0]), &bad_wr)) {
      fprintf(stderr, "Error, ibv_post_send(1) failed roi_id %d\n", roi_id);
      return NULL;
    }
    wrn += h[roi_id];
    debug_printf(
        "transfer starting for roi_id %d %s rashpa_wr %d nImg %d\n", roi_id,
        (link->remu_state == REMU_RUNNING ? "REMU_RUNNING" : "BAD state"),
        rashpa_wr, nImages);
  }

  /**
   * main loop for sending data using 2 lists of work requests
   *
   */

  while (link->remu_state == REMU_RUNNING) {

    // for each ROI post an half frame
    for (int roi_id = 0; roi_id < link->roi_number; roi_id++) {
      // debug_printf("loop in roi_id %d qpn %d wrn
      // %d\n",roi_id,roce->remote_qpn[roi_id],wrn);

      // debug_printf("posting\n");

      if (ibv_post_send(roce->qp[roi_id],
                        (bank ? &wr[roi_id][0] : &wr[roi_id][h[roi_id] / 2]),
                        &bad_wr)) {
        fprintf(stderr, "Error, ibv_post_send(2) failed roi_id %d at %d\n",
                roi_id, bank ? 0 : h[roi_id] / 2);
        return NULL;
      }
      // debug_printf("postesd\n");

      wrn += h[roi_id];
      int nbWr, l;
      l = 0;
      num_comp = 0;
      // extra = 0;
      do {
        l = ibv_poll_cq(roce->cq[roi_id], h[roi_id] / 2 - num_comp, wc);
        num_comp += l;
      } while (num_comp < h[roi_id] / 2);
      wrn -= num_comp;
      nCompletion += num_comp;
      // debug_printf("polled\n");

      /* verify the completion status just to be sure */
      int status = 1; // IBV_WC_SUCCESS;
      status = 1;
      for (int i = 0; i < l; i++) {
        status = status && (wc[i].status == IBV_WC_SUCCESS);
      }
      if (!status) {
        debug_printf("Failed status near %s (%d) for wr_id %d nCompletion %d\n",
                     ibv_wc_status_str(wc[0].status), wc[0].status,
                     (int)wc[0].wr_id, nCompletion);
        exit(0);
      }
      // debug_printf("loop\n");

      // if (link->nanodelay) nanosleep(&delay,&rem);
    }
    // CHECK THIS
    bank = !bank;
    for (int roi_id = 0; roi_id < link->roi_number; roi_id++)
      for (int i = (bank ? 0 : h[roi_id] / 2);
           i < (bank ? h[roi_id] / 2 : h[roi_id]); i++) {
        // debug_printf("create%d\n",i);
        CREATE_WR_DETECTOR;
      }
    counter++;

    if (counter >= nImages) {
      counter = 0;
    }
    // if (pktCounter >> 31) printf("#############     pktCounter overflow
    // ######################\n");
  } // main loop

  for (int roi_id = 0; roi_id < link->roi_number; roi_id++) {
    free(sg[roi_id]);
    free(wr[roi_id]);
  }
  free(wc);
  return NULL;
}
