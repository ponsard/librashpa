/*
 *
 *
 *
 *
 *
 *
 *
 */
#include <arpa/inet.h>
#include <assert.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <zmq.h>

#include "librashpa.h"
#include "rashpa_init.h"
#include "rashpa_zmq.h"

zmq_ctx_t g_zmq;

inline void rashpa_zmq_send_line(void *buffer, int width) {
  zmq_ctx_t *zmq = &g_zmq;
  rashpa_ctx_t *link = &g_rashpa;

  // printf("zmq_send\n");
  zmq_send(zmq->push, buffer, width, 0);
  #ifndef __PPC__
  unsigned long long start = rdtsc(), now;
  if (link->nanodelay)
    do {
      now = rdtsc();
    } while ((now - start) < link->nanodelay);
    #endif
}

inline void rashpa_zmq_buffer_init(void *addr, int w) {}

inline void rashpa_zmq_recv_line(void *buffer, int width) {
  zmq_ctx_t *zmq = &g_zmq;
  zmq_recv(zmq->pull, buffer, width, 0);
}

inline int rashpa_zmq_recv_lines(void *buffer, int width, int n) {
  zmq_ctx_t *zmq = &g_zmq;

  int res = 0;
  for (int i = 0; i < n; i++)
    res += zmq_recv(zmq->pull, buffer + i * width, width, 0);
  return n;
}

void rashpa_zmq_fini(int p) {
  printf("release_zmq_context\n");
  zmq_ctx_t *zmq = &g_zmq;

  if (p == RASHPA_DETECTOR) {
    int rc = zmq_close(zmq->push);
    assert(rc == 0);
  }
  if (p == RASHPA_BACKEND) {
    int rc = zmq_close(zmq->pull);
    assert(rc == 0);
  }

  zmq_ctx_destroy(zmq->context);
}

int rashpa_zmq_init(int dir) {
  zmq_ctx_t *zmq = &g_zmq;
  rashpa_ctx_t *link = &g_rashpa;
  char *url = link->hostname;

  zmq->context = zmq_ctx_new();

  if (dir == RASHPA_DETECTOR) {
    printf("rashpa_zmq_init DETECTOR url %s\n", url);

    zmq->push = zmq_socket(zmq->context, ZMQ_PUSH);
    int value = 128 * 2048 * 2048;
    if (zmq_setsockopt(zmq->push, ZMQ_SNDBUF, &value, sizeof(value)) < 0) {
      perror("setsockopt failed push ZMQ_SNDBUF");
    }
    // value = 1024;
    // if (zmq_setsockopt(zmq->push, ZMQ_SNDHWM,&value, sizeof(value)) < 0) {
    //     perror("setsockopt failed push ZMQ_SNDHWM");
    // }
    // int sndhwm = 0;
    // if (zmq_setsockopt (zmq->push, ZMQ_SNDHWM, &sndhwm, sizeof (sndhwm))) {
    // 	perror("setsockopt failed push ZMQ_SNDHWM");
    // }
    int rc = zmq_bind(zmq->push, url);
    assert(rc == 0);
  } else if (dir == RASHPA_BACKEND) {
    printf("rashpa_zmq_init BACKEND url %s\n", url);

    zmq->pull = zmq_socket(zmq->context, ZMQ_PULL);
    int value = 128 * 2048 * 2048;
    if (zmq_setsockopt(zmq->pull, ZMQ_RCVBUF, &value, sizeof(value)) < 0) {
      perror("setsockopt failed pull ZMQ_RCVBUF");
    }
    // value = 0;
    // if (zmq_setsockopt(zmq->pull, ZMQ_RCVHWM,&value, sizeof(value)) < 0) {
    //     perror("setsockopt failed push ZMQ_RCVHWM");
    // }
    int rc = zmq_connect(zmq->pull, url);
    assert(rc == 0);
  } else
    return RASHPA_LINK_ERROR;
  int timeout = 0;
  zmq_setsockopt(zmq->push, ZMQ_LINGER, &timeout, sizeof(int));
  zmq_setsockopt(zmq->pull, ZMQ_LINGER, &timeout, sizeof(int));

  return RASHPA_LINK_OK;
}
